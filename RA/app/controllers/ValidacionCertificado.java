package controllers;

import play.*;
import play.mvc.*;
import org.apache.commons.codec.binary.Base64;
import java.applet.AppletContext;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.sun.jmx.snmp.BerDecoder;


import models.*;

/**
 * Método para comprobar la validez del certificado del DNI del usuario
 * @author LLuis Santamaria Solà
 *
 */
public class ValidacionCertificado extends Controller {

	public static void index() 
	{
		render();
	}
	
	/*----------------COMPROBACIÓN DE LA VALIDEZ DEL CERTIFICADO--------------------*/
	/**
	 * Certificados de las Autoridades de Certificación subordinadas de la POLICIA NACIONAL
	 */
	final static String sub1 = "C:/Users/Administrador/Documents/TELECOS/4A/cities2/DNIe/subordinada1.crt";
	final static String sub2 = "C:/Users/Administrador/Documents/TELECOS/4A/cities2/DNIe/subordinada2.crt";
	final static String sub3 = "C:/Users/Administrador/Documents/TELECOS/4A/cities2/DNIe/subordinada3.crt";
	
	final static String ruta_redireccion = "";
	
	
	
	public static void obtener_certificado(String cert)
	{
		
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------RA----------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		String cert1 = cert.replaceAll(" ", "+");
		
		System.out.println("CERTIFICADO RECIBIDO----------------------------------------------\n"+cert1);
		byte[] b = cert1.getBytes();
		//System.out.println("Longitud del certificado en bytes:"+b.length+"\n");
		byte[] byteMessage = Base64.decodeBase64(b);
		//System.out.println("received MESSAGE byte Length: "+byteMessage.length+"\n");
		
		 //Guardamos el certificado que llega al RA
			
			try 
			{
				FileOutputStream fos = new FileOutputStream("C:/cer2/certificateRA.cer");
				fos.write(b);
				fos.flush();
				fos.close();
				
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		
		
		Certificate a = null;
		try 
		{
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			
			System.out.println("array input es "+b);
			InputStream in = new ByteArrayInputStream(byteMessage);
			
			a = cf.generateCertificate(in);
			
			
		} 
		
		catch (CertificateException e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		
		X509Certificate certx = (X509Certificate)a;
		
		
		// Ahora que hemos obtenido el certificado, comprobamos su validez
		try {
			System.out.println("Comprobando certificado en RA.. 1/2");
			comprobar_certificado(certx);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Comprobando certificado en RA.. 2/2");
		
		redirect("http://localhost:7777");
		
	}
	
	
	/**
	 * Método para comprobar el certificado del DNI del usuario, descifrando su firma con las diferentes
	 * autoridades de certificación
	 * @param cert_x509
	 * @throws Exception
	 */
	
	private static void comprobar_certificado (X509Certificate cert_x509) throws Exception
	{
			
		//Comprobamos la fecha de expiración del certificado
		int estado = comprobar_validez(cert_x509);
		
		boolean valido = false;

		System.out.println("*----------------------------------------*\n" +
				           "Comprobando certificado de "+cert_x509.getSubjectX500Principal()+
				           "*\n----------------------------------------*\n");

		switch(estado)
		{

			case 0:
				System.out.println("CASO 0");
	
				valido = validar_certificado(cert_x509,sub1)|validar_certificado(cert_x509,sub1)|validar_certificado(cert_x509,sub1);
				break;
	
			case -1:
				valido = false;
				System.out.println("certificado revocado");
				break;
	
			case -2:
				valido =false;
				System.out.println("ERROR");
				break;



		}

		System.out.println("VALIDEZ ES "+valido);
		if(valido)
		{
			System.out.println("Enviamos respuesta a server del concurso");
			//sendPost(0);

			renderText(0);
		}

		else 
		{
			System.out.println("NO ES Válido");
			renderText(-1);
		}
	

	}
	
	
	
	/** La función comprueba la caducidad o invalidez del certificado devuelve:
	 * 
	 * true si todo va bien
	 * false si el certificado está caducado o no es válido
	 * false si ocurre algún problema al validar
	 *
	 *@param cert_x509
	 */
	private static int comprobar_validez(X509Certificate cert_x509)
	{

		Date a;

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();


		try
		{
			cert_x509.checkValidity(date);
		}

		catch(CertificateNotYetValidException e)
		{
			System.out.println("Certificado no valido");
			return -1;

		}
		catch(Exception e)
		{

			return -2;
		}

		return 0;



	}
	
	
	/**-------------------------VERIFICACION CERTIFICADO USUARIO-----------------------------*/
	
	private static boolean validar_certificado(X509Certificate cert_x509,String ruta) throws NoSuchAlgorithmException
	{
		//----OBTENEMOS CERTIFICADO POLICIA-------//
		Certificate cert_policia = null;
		InputStream inStream = null;

		try 
		{
			//Cargamos el certificado de la policia para extraerle la clave pública
			inStream = new FileInputStream(ruta);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			
			//Obtenemos un objeto del tipo Certificate con el certificado de la policía
			cert_policia = cf.generateCertificate(inStream);

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			return false;

		} catch (CertificateException e) {

			e.printStackTrace();
			return false;
		} 

		finally 
		{
			if (inStream != null) 
			{
				try 
				{
					inStream.close();
				} 
				catch (IOException e) 
				{

					e.printStackTrace();

				}
			}
		}
		
		//Convertimos el certificado obtenido en una instancia, X509, para poder manejar 
		//sus datos

		X509Certificate certificado_policia = (X509Certificate)cert_policia;

		//System.out.println("CERT POLICIA---------------------------------*****\n"+certificado_policia.toString());
		//System.out.println("FIN CERT POLICIA---------------------------------*****\n");
		
		/* 
		 * Método alternativo de leer certificado de la policia desde fichero
		 * Certificate cert_police = readWWDRCertificate("C:/cert_policia.cer");
		 */

		

		/* Inicializamos el proceso de verificación de firma */

		//PublicKey Pkp2 = certificado_policia.getPublicKey();
		PublicKey Pkp2 = certificado_policia.getPublicKey();
		//RSAPublicKey Pkr = (RSAPublicKey)certificado_policia.getPublicKey();


		boolean verificado = true;
		String resultado = "Certificado Verificado correctamente";
				
		try
		{
			/*Verificación del certificado con el usuario con la clave pública 
			  del certificado de la policía */
			
			cert_x509.verify(Pkp2);
		}

		catch(NoSuchAlgorithmException n)
		{
			verificado = false;
			System.out.println("     -----ERROR: Algoritmo no encontrado-----");
			
		}
		catch(InvalidKeyException n)
		{
			verificado = false;
			System.out.println("     -----ERROR: Clave inválida-----");
			
		}
		catch(NoSuchProviderException  n)
		{
			verificado = false;
			System.out.println("     -----ERROR: Proveedor no encontrado-----");
			
		}
		catch(SignatureException s)
		{
			verificado = false;
			System.out.println("     -----ERROR: Firma inválida-----");
			
		}
		catch(CertificateException ce)
		{
			verificado = false;
			System.out.println("     -----ERROR: Error en certificado-----");
		}	


		if (verificado == true)
		{ 
			System.out.println("Certificado Verificado correctamente");
			return true;
		}
		else
		{ 
			System.out.println("El certificado no es un certificado emitido por el cuerpo nacional de policía");
			resultado = "El certificado no es un certificado emitido por el cuerpo nacional de policía";
			return false;
		}
		
		
	
	}
	
	private static void sendPost(int resultado) throws Exception 
	{
			
		
		
		System.out.println("enviamos el resultado del a validación...1/2");
		
		//****????*****------PARaMETROS A ENVIAR----------------//
		
		
		
		String urlParameters = "resultado="+Integer.toString(resultado);
			
		//--------URL---------------//
		String url = "http://localhost:9999/resultado_validacion";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//CABECERA
		con.setRequestMethod("POST");
    	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    	
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'POST' a la  URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println("RESPUESTA---------------------------"+response.toString());

		renderText(response.toString());
		
	}

}

