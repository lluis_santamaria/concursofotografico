package models;

import play.*;
import play.mvc.*;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.*;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import java.math.BigInteger;
import java.security.SecureRandom;


import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.signers.PSSSigner;

import models.*;

public class CertificationAuthority extends Controller 
{



	static RSAPrivateKey privCA;
	static RSAPublicKey pubCA;

	static AsymmetricCipherKeyPair clavesCA ;


	private static void generar_claves_RSA()
	{
		/*------------GENERACIÓN DE CLAVES RSA -----------*/

		KeyPairGenerator kpg=null;

		try 
		{
			Security.addProvider(new BouncyCastleProvider());

			kpg = KeyPairGenerator.getInstance("RSA","BC");
			kpg.initialize(1024);

			KeyPair kp = kpg.generateKeyPair();
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

			kpg.initialize(1024, random);

			random = SecureRandom.getInstance("SHA1PRNG", "SUN");

			kpg.initialize(1024, random);

			privCA = (RSAPrivateKey)kp.getPrivate();
			pubCA = (RSAPublicKey)kp.getPublic();

		} 
		catch (NoSuchAlgorithmException | NoSuchProviderException e) 
		{
			// TODO Auto-generated catch blockg
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws InvalidKeyException, NoSuchProviderException, SignatureException
	{
		X509Certificate cert_CA=null;
		try

		{
			
			AsymmetricCipherKeyPair claves = generar_clavesCA(1024);
			System.out.println("Entrem try");
		
			
			//generarCertificadoCA(new Date("25/06/2015"),2333,"dsd");
			cert_CA = genCertCA(claves.getPublic(),claves.getPrivate());
		}
		catch(Exception e )
		{
			System.out.println("Cert null");
		}
	}



	/**
	 * Se recibe la solicitud de generación de certificado con pseudonimo y clave pública
	 * 
	 * @param pseudonimo
	 * @param publica
	 */
	public static void recibir_request_certificado(String pseudonimo, String publica)
	{

		//String cert1 = cert.replaceAll(" ", "+");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("-------------******************************************---------------");
		System.out.println("-------------*                                        *---------------");
		System.out.println("-------------*       *******         *******          *----------------");                                                                  
		System.out.println("-------------*       *******        ***   ***         *---------------");
		System.out.println("-------------*       ***            ***   ***         *---------------");
		System.out.println("-------------*       ***            ***   ***         *---------------");
		System.out.println("-------------*       ***            *********         *---------------");
		System.out.println("-------------*       *******        *********         *---------------");
		System.out.println("-------------*       *******        ***   ***         *---------------");
		System.out.println("-------------*                                        *----------------");
		System.out.println("-------------******************************************---------------");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------\n\n\n");


		System.out.println("****----------------------------------------------------------------****");
		System.out.println("                  Obteniendo propuesta de certificado.......");
		System.out.println("****----------------------------------------------------------------****");


		System.out.println("------------------------------------------------------------------\n");
		System.out.println("             CERTIFICADO RECIBIDO\n\n Pseudonimo: "+pseudonimo+"\n\n");
		System.out.println("------------------------------------------------------------------\n");
	}		


	public static void index() 
	{


		System.out.println("ENTREM INDEX");
		renderText("pipiripiopio?¿?¿¿?¿?¿?????????????????????????????????");
	}


	public static void request_cert(String a)
	{


	}

	public static void cargarCertificadoCA()
	{

	}

	static AsymmetricCipherKeyPair generar_clavesCA(int longitud_clave)
	{
		RSAKeyPairGenerator r = new RSAKeyPairGenerator();

		r.init(new RSAKeyGenerationParameters(new BigInteger("10001", 16), new SecureRandom(),
				longitud_clave, 80));

		AsymmetricCipherKeyPair claves = r.generateKeyPair();
		
		return claves;
		
	}

	@SuppressWarnings("deprecation")
	public static X509Certificate genCertCA(CipherParameters clavepub, CipherParameters clavepriv) throws InvalidKeyException,
	NoSuchProviderException, SignatureException 
	{
		
		

		RSAPrivateKey privl= privCA;
		RSAPublicKey publ = pubCA;
		
		System.out.println("Entrem generacio");



		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();

		System.out.println("Entrem generacio 2 ");
		
		certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
		
		
		System.out.println("Entrem generacio 3");
					
		certGen.setIssuerDN(new X500Principal("CN= AC RAIZ"));
		
		System.out.println("Entrem generacio 4");
		
		certGen.setNotBefore(new Date(System.currentTimeMillis() - 10000));
		
		certGen.setNotAfter(new Date(System.currentTimeMillis() + 10000));
		
		System.out.println("Entrem generacio 5");
		
		certGen.setSubjectDN(new X500Principal("CN=AC RAIZ"));
		
		System.out.println("dp subject, pub CA :"+pubCA.toString());
		certGen.setPublicKey(publ);
		System.out.println("Entrem generacio 6");
		certGen.setSignatureAlgorithm("SHA1WithRSAEncryption");

		System.out.println("Entrem generacio 7");
		
		certGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(false));
		certGen.addExtension(X509Extensions.KeyUsage, true, new KeyUsage(KeyUsage.digitalSignature
				| KeyUsage.keyEncipherment));
		certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(
				KeyPurposeId.id_kp_serverAuth));

		certGen.addExtension(X509Extensions.SubjectAlternativeName, false, new GeneralNames(
				new GeneralName(GeneralName.rfc822Name, "ACRAIZ@certificacion.es")));



		X509Certificate cert =certGen.generateX509Certificate(privl, "BC");

		if(cert== null) System.out.println("CErt null.---\n");
		else{ System.out.println("NOM "+cert.getIssuerDN());}


		return cert;
		}
	}































	/*
	@SuppressWarnings("deprecation")
	public static X509Certificate crearCertificadoCA(PublicKey paramPublicKey1, PrivateKey paramPrivateKey, PublicKey paramPublicKey2)
			throws Exception
			{
		String str = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
		Hashtable localHashtable = new Hashtable();
		Vector localVector = new Vector();
		localHashtable.put(X509Principal.C, "AU");
		localHashtable.put(X509Principal.O, "The Legion of the Bouncy Castle");
		localHashtable.put(X509Principal.L, "Melbourne");
		localHashtable.put(X509Principal.CN, "Eric H. Echidna");
		localHashtable.put(X509Principal.EmailAddress, "feedback-crypto@bouncycastle.org");
		localVector.addElement(X509Principal.C);
		localVector.addElement(X509Principal.O);
		localVector.addElement(X509Principal.L);
		localVector.addElement(X509Principal.CN);
		localVector.addElement(X509Principal.EmailAddress);
		v3CertGen.reset();
		v3CertGen.setSerialNumber(BigInteger.valueOf(20L));
		v3CertGen.setIssuerDN(new X509Principal(str));
		v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 2592000000L));
		v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + 2592000000L));
		v3CertGen.setSubjectDN(new X509Principal(localVector, localHashtable));
		v3CertGen.setPublicKey(paramPublicKey1);
		v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
		v3CertGen.addExtension(MiscObjectIdentifiers.netscapeCertType, false, new NetscapeCertType(48));

		X509Certificate localX509Certificate = v3CertGen.generateX509Certificate(paramPrivateKey);



		byte [] sig = localX509Certificate.getSignature();



		localX509Certificate.checkValidity(new Date());
		localX509Certificate.verify(paramPublicKey2);
		return localX509Certificate;
			}





	 */


















	/* A partir de la clave pública y el pseudónimo elegidos por el usuario, creamos el certificado
	 * que le enviaremos para que pueda realizar votaciones 
	 *
	@SuppressWarnings("deprecation")
	private static X509Certificate generarCertificadoUsuario(Date expiry,int SerialNumber,String dName) throws CertificateEncodingException
	{

		Date startDate = new Date();              
		Date expiryDate = expiry;             
		BigInteger serialNumber = BigInteger.valueOf(3);   
		X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();

		X500Principal   dnName = new X500Principal("CN="+dName);
		X500Principal NameCA = new X500Principal("CN = CA CONCURSO FOTOS");

		certGen.setSerialNumber(serialNumber);
		certGen.setIssuerDN(NameCA);
		certGen.setNotBefore(startDate);
		certGen.setNotAfter(expiryDate);
		certGen.setSubjectDN(dnName);                       
		certGen.setPublicKey(pubCA);

		certGen.setSignatureAlgorithm("SHA1withRSA");
		X509Certificate cert_x509=null;
		try {
			cert_x509 = certGen.generate(privCA, "BC");
		} catch (CertificateEncodingException | InvalidKeyException
				| IllegalStateException | NoSuchProviderException
				| NoSuchAlgorithmException | SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		byte[] certificado_codificado = cert_x509.getEncoded();
		System.out.println("\n\n*****************\n LONGITUD CERTIFICAT ABANS D'ENVIAR: "+certificado_codificado.length);


		FileOutputStream fos;
		try {
			fos = new FileOutputStream("C:/cer/certificatPSEUDO.cer");
			fos.write(certificado_codificado);
			fos.flush();
			fos.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		System.out.println("Finalizando .........\n");
		boolean verificado= true;

		try
		{
			cert_x509.verify(pubCA);
		}
		catch(Exception e)
		{
			System.out.println("ERROR VERIFICACION");
			verificado = false;
		}
		System.out.println("Resultado verificacion:"+verificado);
		return cert_x509;

	}   

	/**
	 * Genera certificado de la autoridad de certificación
	 * 
	 * @param expiry
	 * @param SerialNumber
	 * @param dName
	 * @throws CertificateEncodingException
	 */

	//	@SuppressWarnings("deprecation")
	//	public static void generarCertificadoCA(Date expiry,int SerialNumber,String dName) throws CertificateEncodingException
	//	{
	//		
	//		
	//		  
	//		
	//		/*------------GENERACIÓN DE CLAVES RSA -----------*/
	//		
	//		KeyPairGenerator kpg=null;
	//
	//		try 
	//		{
	//			Security.addProvider(new BouncyCastleProvider());
	//			
	//			kpg = KeyPairGenerator.getInstance("RSA","BC");
	//			kpg.initialize(1024);
	//
	//			KeyPair kp = kpg.generateKeyPair();
	//			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
	//
	//			kpg.initialize(1024, random);
	//
	//			random = SecureRandom.getInstance("SHA1PRNG", "SUN");
	//
	//			kpg.initialize(1024, random);
	//
	//			privCA = (RSAPrivateKey)kp.getPrivate();
	//			pubCA = (RSAPublicKey)kp.getPublic();
	//
	//		} 
	//		catch (NoSuchAlgorithmException | NoSuchProviderException e) 
	//		{
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//
	//		/*--------------------------------------------------*/
	//		
	//		
	//		/*------------GENERACIÓN DE CERTIFICADO---------------*/
	//		
	//		Date startDate = new Date();             
	//		Date expiryDate = expiry;          
	//		BigInteger serialNumber = BigInteger.valueOf(SerialNumber);   
	//				
	//		X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
	//
	//		X500Principal   dnName = new X500Principal("CN="+dName);
	//		X500Principal NameCA = new X500Principal("CN = CA CONCURSO FOTOS");
	//
	//		certGen.setSerialNumber(serialNumber);
	//		certGen.setIssuerDN(NameCA);
	//		certGen.setNotBefore(startDate);
	//		certGen.setNotAfter(expiryDate);
	//		certGen.setSubjectDN(dnName);                       
	//		certGen.setPublicKey(pubCA);
	//
	//		certGen.setSignatureAlgorithm("SHA1withRSA");
	//		X509Certificate cert_x509=null;
	//		try {
	//			cert_x509 = certGen.generate(privCA, "BC");
	//		} catch (CertificateEncodingException | InvalidKeyException
	//				| IllegalStateException | NoSuchProviderException
	//				| NoSuchAlgorithmException | SignatureException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//		/*------------------------------------------------------------*/
	//
	//		
	//		/*-----------ALMACENAMIENTO DE CERTIFICADO--------------------*/
	//		
	//		byte[] certificado_codificado = cert_x509.getEncoded();
	//		
	//		FileOutputStream fos;
	//		try {
	//			fos = new FileOutputStream("C:/cer/certificadoCA.cer");
	//			fos.write(certificado_codificado);
	//			fos.flush();
	//			fos.close();
	//
	//		} catch (FileNotFoundException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		} catch (IOException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//
	//		/*------------------------------------------------------------*/
	//
	//		System.out.println("Finalizando .........\n");
	//		boolean verificado= true;
	//
	//		try
	//		{
	//			cert_x509.verify(pubCA);
	//		}
	//		catch(Exception e)
	//		{
	//			System.out.println("ERROR VERIFICACION");
	//			verificado = false;
	//		}
	//
	//
	//
	//		System.out.println("Resultado verificacion:"+verificado);
	//
	//
	//
	//	}
	//	}

	//	
	//}   










	/*static void main(String[] args) throws CertificateEncodingException, IOException
{

	System.out.println("Elija una opción :\n");
	System.out.println("Opción 1: Generar certificado CA");
	System.out.println("Opción 2: Verificar certificado");
	boolean salir = false;
	while(!salir)
	{
		int resultado = System.in.read();


		switch(resultado)
		{

			case 1:

			generarCertificadoCA(new Date("26/05/2014"),1,"CA CONCURSO FOTOS");
			break;

			case 0:
				salir = true;
				break;


		}

	}
	System.out.println("ADIÓS");


}*/















	/*byte[] b = cert.getBytes();
		//System.out.println("Longitud del certificado en bytes:"+b.length+"\n");
		byte[] byteMessage = Base64.decodeBase64(b);
		//System.out.println("mensaje recibido byte Length: "+byteMessage.length+"\n");

		System.out.println("");


			try {
				FileOutputStream fos = new FileOutputStream("C:/cer2/certificateRA.cer");
				fos.write(b);
				fos.flush();
				fos.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}



		Certificate a = null;
		try 
		{
			CertificateFactory cf = CertificateFactory.getInstance("X.509");

			System.out.println("array input es "+b);
			InputStream in = new ByteArrayInputStream(byteMessage);

			a = cf.generateCertificate(in);


		} 

		catch (CertificateException e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		}


		X509Certificate certx = (X509Certificate)a;


		// Ahora que hemos obtenido el certificado, comprobamos su validez
		try {
			//comprobar_certificado(certx);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


    }

	 */


