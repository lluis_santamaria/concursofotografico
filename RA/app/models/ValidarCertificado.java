package models;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ValidarCertificado 
{
	
	protected void comprobar_certificado (X509Certificate cert_x509) throws Exception
	{

		int estado = comprobar_validez(cert_x509);
		//int estado = 0;
		int valido=-1;
		System.out.println("SWITCH ESTADO");

		System.out.println("Comprobando certificado "+cert_x509.getSubjectX500Principal());

		switch(estado)
		{




		case 0:
			System.out.println("CASO 0");

			valido = validar_certificado(cert_x509);
			break;

		case -1:
			valido = -1;
			System.out.println("certificado revocado");
			break;

		case -2:
			valido =-2;
			System.out.println("ERROR");
			break;



		}

		if(valido == 0)
		{
			//sendPost(cert_x509);

		}

		else 
			System.out.println("NO ES Válido");


	}

	protected int comprobar_validez(X509Certificate cert_x509)
	{
		//------------COMPROBAR VALIDEZ CERTIFICADO-----------------------------------//
		/* La función devuelve:
		 * 0 si todo va bien
		 * -1 si el certificado está caducado o no es válido
		 * -2 si ocurre algún problema al validar
		 *
		 */


		Date a;

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();


		try
		{
			cert_x509.checkValidity(date);
		}

		catch(CertificateExpiredException e)
		{
			System.out.println("Su certificado esta expirado");

			return -1;

		}

		catch(CertificateNotYetValidException e)
		{
			System.out.println("Certificado no valido 2");
			return -1;

		}
		catch(Exception e)
		{

			return -2;
		}

		return 0;



	}


	protected int validar_certificado(X509Certificate cert_x509) throws NoSuchAlgorithmException
	{

		/**-------------------------VERIFICACION CERTIFICADO USUARIO--------------------------------------**/

		System.out.println("CERT NO EXPIRADO\n");
		//----OBTENEMOS FIRMA DEL CERTIFICADO DEL USUARIO-------//
		Certificate cert_policia = null;
		InputStream inStream = null;

		try 
		{
			//Cargamos el certificado de la policia para extraerle la clave pública
			inStream = new FileInputStream("C:/Users/Administrador/Documents/TELECOS/4A/cities2/certificado_subordinada3.cer");
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			cert_policia = cf.generateCertificate(inStream);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -2;

		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -3;
		} 

		finally 
		{
			if (inStream != null) 
			{
				try 
				{
					inStream.close();
				} 
				catch (IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
		}

		X509Certificate certificado_policia = (X509Certificate)cert_policia;

		System.out.println("CERT POLICIA---------------------------------*****\n"+certificado_policia.toString());
		System.out.println("FIN CERT POLICIA---------------------------------*****\n");
		/* 
		 * Método alternativo de leer certificado de la policia desde fichero
		 * Certificate cert_police = readWWDRCertificate("C:/cert_policia.cer");
		 */

		//System.out.println("nom es "+cert_policia.getIssuerDN());


		/* Inicializamos el proceso de verificación de firma */



		//PublicKey Pkp2 = certificado_policia.getPublicKey();
		PublicKey Pkp2 = certificado_policia.getPublicKey();
		//RSAPublicKey Pkr = (RSAPublicKey)certificado_policia.getPublicKey();


		boolean verificado = true;
		// System.out.println("clau"+Pku);
		System.out.println("\n\n\n\nclau");

		System.out.println("abans try");
		try

		{
			//RSAPublicKey pub = (RSAPublicKey) certificado_policia.getPublicKey();
			cert_x509.verify(Pkp2);
		}

		catch(NoSuchAlgorithmException n)
		{
			verificado = false;
			System.out.println("entrem catch no algorithm");
		}
		catch(InvalidKeyException n)
		{
			verificado = false;
			System.out.println("entrem catch invalid key");
		}
		catch(NoSuchProviderException  n)
		{
			verificado = false;
			System.out.println("entrem catch provider");
		}
		catch(SignatureException s)
		{
			verificado = false;
			System.out.println("entrem catch signature");
		}
		catch(CertificateException ce)
		{
			verificado = false;
			System.out.println("entrem catch certificate");
		}	


		if (verificado == true)
		{ 
			System.out.println("Certificado Verificado correctamente");
			return 0;
		}
		else
		{ 
			System.out.println("El certificado no es un certificado válido firmado por el cuerpo nacional de policía");

			return -9;
		}









	}


}
