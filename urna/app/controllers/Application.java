package controllers;

import play.*;
import play.mvc.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;

import models.*;

/**
 * Servidor de urna. Verifica que el usuario tenga un certificado válido (firmado por la CA)
 * 
 * @author LLuís Santamaria Solà
 *
 */
public class Application extends Controller 
{

    public static void index2() 
    {
        render();
    }
    
    /**
     * Rutas de guardado y conexión
     */
    final static String ruta_CA = "http://192.168.1.135:7777/CertificationAuthority/request_publica";
    final static String ruta_votos = "C:/urna";
    
    
    /**
     * Método para extraer el pseudonimo y la foto firmada del voto recibido
     * @param votoCoded
     * @throws IOException
     */
    
    public static void recibir_voto(String votoCoded) throws IOException
    {
    	Votos voto = new Votos();
    	
    	String[] part1 = votoCoded.split("<Pseudonimo>");
    	
    	String[] part2 = part1[1].split("</Pseudonimo>");
    	
    	
    	String Pseudonimo = part2[0];
    	
    	
    	
    	String[] part3 = votoCoded.split("<FotoVotada>");
    	
    	String[] part4 = part3[1].split("</FotoVotada>");
    	
    	
    	String fotoVotada = part4[0];
    	
    	
    	//Guardamos el voto!
    	voto.setFoto_votada(Integer.parseInt(fotoVotada));
    	voto.setPseudonim(Pseudonimo);
    	voto.save();
    	
    }
    
    /**
     * Renderiza la plantilla de urna con una lista de los votos
     */
    public static void index()
    {
    	
    	List<Votos> votos = Votos.findAll();
    	
    	render(votos);
    	
    	
    }
    
    
    /**
     * Verifica la firma del certificado del usuario descegado. Comprueba que esté firmado
     * por la CA con la clave publica
     * @param certFirmadoCoded
     * @param certOrig
     * @throws Exception
     */
    
    public static void verificar_firma(String certFirmadoCoded,String certOrig) throws Exception
    {
    	certFirmadoCoded = certFirmadoCoded.replaceAll(" ", "+");
    	System.out.println("--------------URNA--------------\n CER Firmado :"+certFirmadoCoded+"----Cert_Original"+certOrig);
    	boolean verificacion_firma =true;
    	PublicKey pk = obtener_publica_CA();
    	CipherParameters pC = convertir_publica(pk);
    	byte[] certDecoded = Base64.decodeBase64(certFirmadoCoded);
    	System.out.println("Verificanod..");
    	
    	verificacion_firma = Cegado.verify(pC,  Base64.decodeBase64(certOrig.getBytes()),certDecoded);
		
		System.out.println("\n URNA USER VERIFICACION : "+true+"-----------------\n");
    	renderText("OK");
    }
    private static CipherParameters convertir_publica(PublicKey p)
	{
		// Convertimos la clave pública a un objeto RSAPublicKey para poder obtener su módulo y su exponente
		RSAPublicKey RSAp = (RSAPublicKey)p;

		//Creamos e instanciamos un objeto del tipo RSAKeyParameters con el módulo y exponente de la clave 
		// publica para poder generar el factor de cegado basándonos en ella a través de las librerías de BC
		RSAKeyParameters publica = new RSAKeyParameters(false,RSAp.getModulus(),RSAp.getPublicExponent());

		CipherParameters publicaC  = (CipherParameters)publica;


		return publicaC;
	}
    private static PublicKey obtener_publica_CA() throws Exception
	{

		String publica = requestPublicaCA();
		
		System.out.println("publica que recibe user  "+publica);
		byte [] publicadec = Base64.decodeBase64(publica);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicadec);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    PublicKey pubKey =  keyFactory.generatePublic(pubKeySpec);

		//System.out.println("PUBLICA OBTENIDA USER: "+publica);
		
		return pubKey;
	}
    
    private static String requestPublicaCA() throws Exception 
	{
			
		//****????*****------PARaMETROS A ENVIAR----------------//
				
		//String urlParameters = certCegado;
			
		//--------URL---------------//
		String url = ruta_CA;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		//CABECERA
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'GET' a la  URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		String clave = response.toString().replace(" ", "+");
		//print result
		System.out.println("????????PUBLICA LONG : "+clave.length()+"\nRESPUESTA---------------------------"+response.toString());
	
		
		return response.toString();
		
	}

}