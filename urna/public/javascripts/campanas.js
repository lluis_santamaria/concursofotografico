// JavaScript Document
$(document).ready(function() {
	$("#result").css("display", "none"); 
}
)
function submitform(action)
{
      $('#result').html("");     
	 
	if (action == 'new'){
	
     $.post("add_campaign.php",
      { id: $('#id').val(), 
        client: $('#client').val(),
        subject: $('#subject').val(),
        editorCamp: $('#editorCamp').val(),
        type_cost: $('#type_cost').val(),
        cost: $('#cost').val(),
        active: $('#active').val(),
		urlLanding:	$('#urlLanding').val(),
		typeCamp: $('#typeCamp').val()
       },function(respuesta){
		$("#result").css("display", "block"); 
        $('#result').html(respuesta);
      });
    }else{
		if (action == 'update') {
			 $('#result').html(""); 
			$.post("upd_campaign.php",
			  { id: $('#id').val(), 
				client: $('#client').val(),
				subject: $('#subject').val(),
				editorCamp: $('#editorCamp').val(),
				type_cost: $('#type_cost').val(),
				cost: $('#cost').val(),
				active: $('#active').val(),
				urlLanding:	$('#urlLanding').val()	
			   },function(respuesta){
				$("#result").css("display", "block"); 
				$('#result').html(respuesta);
			  });
		}
	} 
         
}

function DeleteCamp (idCamp){
 if (confirm("Seguro que deasea borrar la campana "+idCamp+" y toda su informacion?")){
	
	$.ajax({
		type:"POST",
		url: "delete.php",
		data:{
			id: idCamp
		},
		success: function (respuesta){
		
			location.reload();
		}
	})
 }
 
}