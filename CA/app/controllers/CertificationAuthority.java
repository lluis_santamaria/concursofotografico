package controllers;

import play.*;
import play.mvc.*;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;

import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;


import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.signers.PSSSigner;

import models.*;



public class CertificationAuthority extends Controller 
{
	
	/**------VARIABLES PARA CREACIÓN DE CERTIFICADO CA---------*/

	final static String ruta_guardado_cert_CA = "C:/cer/certificatCA.cer";
	final static String fecha_expiracion_cert_CA = "20/09/2014";
	final static String ruta_guardado_clave_CA = "C:/cer/publica.cer";

	/**------VARIABLES PARA MANIPULACIÓN DE KEYSTORE---------*/

	final static char[] password_KeyStore = "000000".toCharArray();
	final static char[] password_privada_CA = "111111".toCharArray();
	final static String nombre_privada_CA = "ClavePrivada";

	
	static PublicKey pubCA;
	static PrivateKey privCA;
	
	
	static CipherParameters pubCACipher;
	static CipherParameters privCACipher;

	static AsymmetricCipherKeyPair clavesCA ;

	

	final static String ruta_server_concurso = "http://localhost:9999/recibir_certFirmado_concurso";
	
	
	public static void genera_claves() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
	{
		System.out.println("------GENERAMOS CLAVES CA");
		generacion_claves_CA();
	}
	
	
	public static void request_publica() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, IOException
	{
		
		
		System.out.println("-------------------------CA-----------------\n");
		
		System.out.println("Enviando clave publica de CA a usuario...\n");
		
		PublicKey pk = obtener_publica_CA();
		
		byte[] pkB = Base64.encodeBase64(pk.getEncoded());
		
		System.out.println("Longitud------???? : "+new String(pkB).length()+"\n\nCLAVE PUBLICA...\n"+pkB);
		renderText(new String(pkB,"UTF-8"));
		
	}

	private static PublicKey obtener_publica_CA() throws NoSuchAlgorithmException, IOException, 
	NoSuchProviderException,InvalidKeySpecException
	{

		

		/*FileInputStream keyfis = new FileInputStream(ruta_guardado_clave_CA);


		byte[] claveCodificada = new byte[keyfis.available()]; 

		//byte[] clavedescodificada = Base64.decodeBase64(claveCodificada);
		keyfis.read(claveCodificada);
		keyfis.close();

		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(claveCodificada);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");



		PublicKey pubKey =  keyFactory.generatePublic(pubKeySpec);*/
		
		


		return pubCA;
	}
	
	public static void recibir_certCegado_CA(String certCegadoB64) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, UnsupportedEncodingException
	{
		
		
	    System.out.println("----------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("-------------******************************************---------------");
		System.out.println("-------------*                                        *---------------");
		System.out.println("-------------*       *******         *******          *----------------");                                                                  
		System.out.println("-------------*       *******        ***   ***         *---------------");
		System.out.println("-------------*       ***            ***   ***         *---------------");
		System.out.println("-------------*       ***            ***   ***         *---------------");
		System.out.println("-------------*       ***            *********         *---------------");
		System.out.println("-------------*       *******        *********         *---------------");
		System.out.println("-------------*       *******        ***   ***         *---------------");
		System.out.println("-------------*                                        *----------------");
		System.out.println("-------------******************************************---------------");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------\n\n\n");


		System.out.println("****----------------------------------------------------------------****");
		System.out.println("                  Obteniendo propuesta de certificado.......");
		System.out.println("****----------------------------------------------------------------****");


	

	

		String certCegado = certCegadoB64.replaceAll(" ", "+");
		
		System.out.println("------------------------------------------------------------------\n");
		System.out.println("             CERTIFICADO RECIBIDO\n\n  "+certCegado+"\n\n");
		System.out.println("------------------------------------------------------------------\n");
		

		byte[] certCegadodescB64 = Base64.decodeBase64(certCegadoB64);

		//		System.out.println("\n CA: Certificado decoded B64 \n "+certCegadodescB64);
		//		
		//		System.out.println("\n CA: Longitud Certificado decodeddB64 bytes: "+certCegadoB64.length());

		System.out.println("********FIRMA DEL CERTIFICADO CEGADO EN CA********\n\n");
//		
//	
//
	   	byte[] cert_firmado = firmaCertCegadoCA(certCegadodescB64);

		//System.out.println("\n CA: Certificado Firmado: "+cer);
		//System.out.println("\n CA: Longitud Certificado Firmado: "+cert_firmado.length);
//		
		byte[] certCegadoFirmadoB64 = Base64.encodeBase64(cert_firmado);
		System.out.println("\n------Enviando Cert Cegado Firmado a CONCURSO \n"+certCegadoFirmadoB64);
		renderText(new String(certCegadoFirmadoB64,"UTF-8"));
		
//		//enviar_certFirmadoConcurso(cert_firmado);
	}
	
	static byte[] firmaCertCegadoCA(byte[] certCegado)
	{
			System.out.println("\n\n***-------------------PROCESO DE FIRMA CIEGA CA ---------------------***\n\n");

			
			//------------- CA: Verifica la firma de user -------------//
			//        if (Cegado.verify(pubkeyuser, blinded_cert_a_cegar, sig)) {

			//---------- CA: Firma el cert cegado con su clave privada ----------//
			byte[] certFirmado = Cegado.signBlinded(privCACipher, certCegado);


			return certFirmado;


	}
	
	
	/*--------MÉTODOS DE UN SOLO USO PARA OBTENER UN CERTIFICADO "SELF-SIGNED" PARA SU USO COMO CA------*/

	public static void generar_certificado_CA() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, InvalidKeyException, CertificateEncodingException, SignatureException
	{
		
		KeyPair k = generacion_claves_CA();
		
		X509Certificate x_509 = genCertCA(k.getPublic(),k.getPrivate());
		
		guardar_certCA(x_509);
		
		
		
		
		
		
		
	}
	
	
	private static KeyPair generacion_claves_CA() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
	{

		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");


		RSAKeyPairGenerator gen = new RSAKeyPairGenerator();
		gen.init(new RSAKeyGenerationParameters(BigInteger.valueOf(3), sr, 1024, 80));
		AsymmetricCipherKeyPair keypair = gen.generateKeyPair();
		RSAKeyParameters publicKey = (RSAKeyParameters) keypair.getPublic();
		RSAPrivateCrtKeyParameters privateKey = (RSAPrivateCrtKeyParameters) keypair.getPrivate();

		/* Definimos una estructura de la clave pública del tipo RSA, que estará constituida por el módulo común con
           la clave privada, y un exponente característico y unívoco propio de la pública */

		RSAPublicKeyStructure pkStruct = new RSAPublicKeyStructure(publicKey.getModulus(), publicKey.getExponent());

		/* Hasta ahora hemos trabajado con "sintaxis/terminología/objetos" de las librerías BouncyCastle. Para poder trabajar
		    con las librerías de JCE, necesitamos hacer una conversión en las claves */

		// formato JCE 
		PublicKey pubKey;
		PrivateKey privKey;


		pubKey = KeyFactory.getInstance("RSA").generatePublic(
				new RSAPublicKeySpec(publicKey.getModulus(), publicKey.getExponent()));
		// and this one for the KeyStore
		privKey = KeyFactory.getInstance("RSA").generatePrivate(
				new RSAPrivateCrtKeySpec(publicKey.getModulus(), publicKey.getExponent(),
						privateKey.getExponent(), privateKey.getP(), privateKey.getQ(), 
						privateKey.getDP(), privateKey.getDQ(), privateKey.getQInv()));
		
		pubCA = pubKey;
		privCA= privKey;
		pubCACipher = keypair.getPublic();
		privCACipher = keypair.getPrivate();
		return new KeyPair(pubKey,privKey); 
	}

	@SuppressWarnings("deprecation")

	/** 
	 * Método para generar el certificado de CA. Este método sólo se usará una vez, ya que 
	 * es un certificado autofirmado por la CA mismo y que se mantendrá hasta el final del
	 * concurso.
	 * 
	 * @param clavepub 
	 * @param clavepriv
	 * @return cert
	 * @throws InvalidKeyException
	 * @throws NoSuchProviderException
	 * @throws SignatureException
	 * 
	 */

	private static X509Certificate genCertCA(PublicKey pubKey,PrivateKey privKey) throws InvalidKeyException,
	NoSuchProviderException, SignatureException, CertificateEncodingException 
	{

		System.out.println("Generando certificado CA...");

		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();

		certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));

		certGen.setIssuerDN(new X500Principal("CN= AC RAIZ"));

		System.out.println("Entrem generacio 4");

		certGen.setNotBefore(new Date(System.currentTimeMillis() - 10000));

		certGen.setNotAfter(new Date(fecha_expiracion_cert_CA));

		certGen.setSubjectDN(new X500Principal("CN=AC RAIZ"));

		certGen.setPublicKey(pubKey);

		certGen.setSignatureAlgorithm("SHA1WithRSAEncryption");

		certGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(false));
		certGen.addExtension(X509Extensions.KeyUsage, true, new KeyUsage(KeyUsage.digitalSignature
				| KeyUsage.keyEncipherment));
		certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(
				KeyPurposeId.id_kp_serverAuth));

		certGen.addExtension(X509Extensions.SubjectAlternativeName, false, new GeneralNames(
				new GeneralName(GeneralName.rfc822Name, "ACRAIZ@certificacion.es")));

		X509Certificate cert =certGen.generateX509Certificate(privKey, "BC");


		return cert;



	}
	private static void guardar_certCA(X509Certificate cert_x509) throws CertificateEncodingException
	{

		byte[] certificado_codificado = Base64.encodeBase64(cert_x509.getEncoded());

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(ruta_guardado_cert_CA);
			fos.write(certificado_codificado);
			fos.flush();
			fos.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		System.out.println("CERT STRING---------------------------------\n\n\n");
		guardar_clavePublica(cert_x509.getPublicKey());


	}

	private static void guardar_clavePublica(PublicKey pub) throws CertificateEncodingException
	{

		//byte[] clave_codificada = Base64.encodeBase64(pub.getEncoded());
		byte[] clave_codificada = pub.getEncoded();
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(ruta_guardado_clave_CA);
			fos.write(clave_codificada);
			fos.flush();
			fos.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*private static void guardar_clavePrivada(PrivateKey priv,X509Certificate cert) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
	{

		KeyStore ks = KeyStore.getInstance("PKCS12");
		ks.load(null, password_KeyStore);
		KeyStore.ProtectionParameter protParam =
				new KeyStore.PasswordProtection(password_privada_CA);

		// En este caso, al ser un certificado propio, la cadena de certificación solo 
		// contendrá una entrada
		Certificate[] certificate_chain = new Certificate[1];
		certificate_chain[0] = cert;


		//Introducimos la clave privada en el almacén de claves en formato PKCS12

		KeyStore.PrivateKeyEntry EntryPrivada = new PrivateKeyEntry(priv,certificate_chain);

		ks.setEntry(nombre_privada_CA, EntryPrivada, protParam);
	}*/
	
}
//	
//	
//	
//
//	private static void generar_claves_RSA()
//	{
//		/*------------GENERACIÓN DE CLAVES RSA -----------*/
//
//		KeyPairGenerator kpg=null;
//
//		try 
//		{
//			Security.addProvider(new BouncyCastleProvider());
//
//			kpg = KeyPairGenerator.getInstance("RSA","BC");
//			kpg.initialize(1024);
//
//			KeyPair kp = kpg.generateKeyPair();
//			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//
//			kpg.initialize(1024, random);
//
//			random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//
//			kpg.initialize(1024, random);
//
//			privCA = (RSAPrivateKey)kp.getPrivate();
//			pubCA = (RSAPublicKey)kp.getPublic();
//
//		} 
//		catch (NoSuchAlgorithmException | NoSuchProviderException e) 
//		{
//			// TODO Auto-generated catch blockg
//			e.printStackTrace();
//		}
//	}
//
//	public static void main(String[] args) throws InvalidKeyException, NoSuchProviderException, SignatureException
//	{
//		X509Certificate cert_CA=null;
//		try
//
//		{
//			
//			AsymmetricCipherKeyPair claves = generar_clavesCA(1024);
//			System.out.println("Entrem try");
//		
//			
//			//generarCertificadoCA(new Date("25/06/2015"),2333,"dsd");
//			cert_CA = genCertCA(claves.getPublic(),claves.getPrivate());
//		}
//		catch(Exception e )
//		{
//			System.out.println("Cert null");
//		}
//	}
//
//
//
//	
//	
	

	
//	static void enviar_certFirmadoConcurso(byte[] certFirmado)
//	{
//		System.out.println("CA:Enviamos certificado al SERVIDOR CONCURSO");
//
//		String certFirmadoB64 = Base64.encodeBase64String(certFirmado);
//
//		System.out.println("CA: Longitud de certFirmadoB64 a enviar: \n "+certFirmadoB64.length());
//		System.out.println("CA: CertFirmadoB64 a enviar \n "+certFirmadoB64);
//
//		sendPostCertCegadoaConcurso(certFirmadoB64);
//		
//		
//		
//		
//	}
//		
//	
//	
//	
  
//
//
//	public static void index() 
//	{
//
//
//		System.out.println("ENTREM INDEX");
//		renderText("pipiripiopio?¿?¿¿?¿?¿?????????????????????????????????");
//	}
//
//
//	public static void request_cert(String a)
//	{
//
//
//	}
//
//	public static void cargarCertificadoCA()
//	{
//
//	}
//
//	static AsymmetricCipherKeyPair generar_clavesCA(int longitud_clave)
//	{
//	
//		RSAKeyPairGenerator r = new RSAKeyPairGenerator();
//
//		r.init(new RSAKeyGenerationParameters(new BigInteger("10001", 16), new SecureRandom(),
//				longitud_clave, 80));
//
//		AsymmetricCipherKeyPair claves = r.generateKeyPair();
//		
//		return claves;
//		
//	}
//
//	@SuppressWarnings("deprecation")
//	public static X509Certificate genCertCA(CipherParameters clavepub, CipherParameters clavepriv) throws InvalidKeyException,
//	NoSuchProviderException, SignatureException 
//	{
//	
//		RSAPrivateKey privl= privCA;
//		RSAPublicKey publ = pubCA;
//		
//		System.out.println("Entrem generacio");
//
//		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//
//		X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
//
//		System.out.println("Entrem generacio 2 ");
//		
//		certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
//		
//		
//		System.out.println("Entrem generacio 3");
//					
//		certGen.setIssuerDN(new X500Principal("CN= AC RAIZ"));
//		
//		System.out.println("Entrem generacio 4");
//		
//		certGen.setNotBefore(new Date(System.currentTimeMillis() - 10000));
//		
//		certGen.setNotAfter(new Date(System.currentTimeMillis() + 10000));
//		
//		System.out.println("Entrem generacio 5");
//		
//		certGen.setSubjectDN(new X500Principal("CN=AC RAIZ"));
//		
//		System.out.println("dp subject, pub CA :"+pubCA.toString());
//		certGen.setPublicKey(publ);
//		System.out.println("Entrem generacio 6");
//		certGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
//
//		System.out.println("Entrem generacio 7");
//		
//		certGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(false));
//		certGen.addExtension(X509Extensions.KeyUsage, true, new KeyUsage(KeyUsage.digitalSignature
//				| KeyUsage.keyEncipherment));
//		certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(
//				KeyPurposeId.id_kp_serverAuth));
//
//		certGen.addExtension(X509Extensions.SubjectAlternativeName, false, new GeneralNames(
//				new GeneralName(GeneralName.rfc822Name, "ACRAIZ@certificacion.es")));
//
//
//		X509Certificate cert =certGen.generateX509Certificate(privl, "BC");
//
//		if(cert== null) System.out.println("CErt null.---\n");
//		else{ System.out.println("NOM "+cert.getIssuerDN());}
//
//		return cert;
//		
//		}
//	
//
//
//	private static void sendPostCertCegadoaConcurso(String certCegado) throws Exception 
//	{
//			
//		//****????*****------PARaMETROS A ENVIAR----------------//
//				
//		String urlParameters = certCegado;
//			
//		//--------URL---------------//
//		String url = ruta_server_concurso;
//		
//		URL obj = new URL(url);
//		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//	
//		//CABECERA
//		con.setRequestMethod("POST");
//		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//		
//		// Envío POST
//		con.setDoOutput(true);
//		con.setFollowRedirects(true);
//		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//		
//		//ANADIENDO PARAMETROS Y ENVIANDO
//		wr.writeBytes(urlParameters);
//		wr.flush();
//		wr.close();
//		con.setFollowRedirects(true);
//		
//		//RECIBIENDO RESPUESTA
//		int responseCode = con.getResponseCode();
//		System.out.println("\nEnviando 'POST' a la  URL : " + url);
//		//System.out.println("Post parameters : " + urlParameters);
//		System.out.println("Código respuesta : " + responseCode);
//		
//		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
//		String inputLine;
//		StringBuffer response = new StringBuffer();
//	
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//	
//		//print result
//		System.out.println("RESPUESTA---------------------------"+response.toString());
//	
//		renderText(response.toString());
//		
//	}
//}
//	/*
//	@SuppressWarnings("deprecation")
//	public static X509Certificate crearCertificadoCA(PublicKey paramPublicKey1, PrivateKey paramPrivateKey, PublicKey paramPublicKey2)
//			throws Exception
//			{
//		String str = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
//		Hashtable localHashtable = new Hashtable();
//		Vector localVector = new Vector();
//		localHashtable.put(X509Principal.C, "AU");
//		localHashtable.put(X509Principal.O, "The Legion of the Bouncy Castle");
//		localHashtable.put(X509Principal.L, "Melbourne");
//		localHashtable.put(X509Principal.CN, "Eric H. Echidna");
//		localHashtable.put(X509Principal.EmailAddress, "feedback-crypto@bouncycastle.org");
//		localVector.addElement(X509Principal.C);
//		localVector.addElement(X509Principal.O);
//		localVector.addElement(X509Principal.L);
//		localVector.addElement(X509Principal.CN);
//		localVector.addElement(X509Principal.EmailAddress);
//		v3CertGen.reset();
//		v3CertGen.setSerialNumber(BigInteger.valueOf(20L));
//		v3CertGen.setIssuerDN(new X509Principal(str));
//		v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 2592000000L));
//		v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + 2592000000L));
//		v3CertGen.setSubjectDN(new X509Principal(localVector, localHashtable));
//		v3CertGen.setPublicKey(paramPublicKey1);
//		v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
//		v3CertGen.addExtension(MiscObjectIdentifiers.netscapeCertType, false, new NetscapeCertType(48));
//
//		X509Certificate localX509Certificate = v3CertGen.generateX509Certificate(paramPrivateKey);
//
//
//
//		byte [] sig = localX509Certificate.getSignature();
//
//
//
//		localX509Certificate.checkValidity(new Date());
//		localX509Certificate.verify(paramPublicKey2);
//		return localX509Certificate;
//			}
//
//
//
//
//
//	 */
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//	/* A partir de la clave pública y el pseudónimo elegidos por el usuario, creamos el certificado
//	 * que le enviaremos para que pueda realizar votaciones 
//	 *
//	@SuppressWarnings("deprecation")
//	private static X509Certificate generarCertificadoUsuario(Date expiry,int SerialNumber,String dName) throws CertificateEncodingException
//	{
//
//		Date startDate = new Date();              
//		Date expiryDate = expiry;             
//		BigInteger serialNumber = BigInteger.valueOf(3);   
//		X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
//
//		X500Principal   dnName = new X500Principal("CN="+dName);
//		X500Principal NameCA = new X500Principal("CN = CA CONCURSO FOTOS");
//
//		certGen.setSerialNumber(serialNumber);
//		certGen.setIssuerDN(NameCA);
//		certGen.setNotBefore(startDate);
//		certGen.setNotAfter(expiryDate);
//		certGen.setSubjectDN(dnName);                       
//		certGen.setPublicKey(pubCA);
//
//		certGen.setSignatureAlgorithm("SHA1withRSA");
//		X509Certificate cert_x509=null;
//		try {
//			cert_x509 = certGen.generate(privCA, "BC");
//		} catch (CertificateEncodingException | InvalidKeyException
//				| IllegalStateException | NoSuchProviderException
//				| NoSuchAlgorithmException | SignatureException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//		byte[] certificado_codificado = cert_x509.getEncoded();
//		System.out.println("\n\n*****************\n LONGITUD CERTIFICAT ABANS D'ENVIAR: "+certificado_codificado.length);
//
//
//		FileOutputStream fos;
//		try {
//			fos = new FileOutputStream("C:/cer/certificatPSEUDO.cer");
//			fos.write(certificado_codificado);
//			fos.flush();
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//		System.out.println("Finalizando .........\n");
//		boolean verificado= true;
//
//		try
//		{
//			cert_x509.verify(pubCA);
//		}
//		catch(Exception e)
//		{
//			System.out.println("ERROR VERIFICACION");
//			verificado = false;
//		}
//		System.out.println("Resultado verificacion:"+verificado);
//		return cert_x509;
//
//	}   
//
//	/**
//	 * Genera certificado de la autoridad de certificación
//	 * 
//	 * @param expiry
//	 * @param SerialNumber
//	 * @param dName
//	 * @throws CertificateEncodingException
//	 */
//
//	//	@SuppressWarnings("deprecation")
//	//	public static void generarCertificadoCA(Date expiry,int SerialNumber,String dName) throws CertificateEncodingException
//	//	{
//	//		
//	//		
//	//		  
//	//		
//	//		/*------------GENERACIÓN DE CLAVES RSA -----------*/
//	//		
//	//		KeyPairGenerator kpg=null;
//	//
//	//		try 
//	//		{
//	//			Security.addProvider(new BouncyCastleProvider());
//	//			
//	//			kpg = KeyPairGenerator.getInstance("RSA","BC");
//	//			kpg.initialize(1024);
//	//
//	//			KeyPair kp = kpg.generateKeyPair();
//	//			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//	//
//	//			kpg.initialize(1024, random);
//	//
//	//			random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//	//
//	//			kpg.initialize(1024, random);
//	//
//	//			privCA = (RSAPrivateKey)kp.getPrivate();
//	//			pubCA = (RSAPublicKey)kp.getPublic();
//	//
//	//		} 
//	//		catch (NoSuchAlgorithmException | NoSuchProviderException e) 
//	//		{
//	//			// TODO Auto-generated catch block
//	//			e.printStackTrace();
//	//		}
//	//
//	//		/*--------------------------------------------------*/
//	//		
//	//		
//	//		/*------------GENERACIÓN DE CERTIFICADO---------------*/
//	//		
//	//		Date startDate = new Date();             
//	//		Date expiryDate = expiry;          
//	//		BigInteger serialNumber = BigInteger.valueOf(SerialNumber);   
//	//				
//	//		X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
//	//
//	//		X500Principal   dnName = new X500Principal("CN="+dName);
//	//		X500Principal NameCA = new X500Principal("CN = CA CONCURSO FOTOS");
//	//
//	//		certGen.setSerialNumber(serialNumber);
//	//		certGen.setIssuerDN(NameCA);
//	//		certGen.setNotBefore(startDate);
//	//		certGen.setNotAfter(expiryDate);
//	//		certGen.setSubjectDN(dnName);                       
//	//		certGen.setPublicKey(pubCA);
//	//
//	//		certGen.setSignatureAlgorithm("SHA1withRSA");
//	//		X509Certificate cert_x509=null;
//	//		try {
//	//			cert_x509 = certGen.generate(privCA, "BC");
//	//		} catch (CertificateEncodingException | InvalidKeyException
//	//				| IllegalStateException | NoSuchProviderException
//	//				| NoSuchAlgorithmException | SignatureException e) {
//	//			// TODO Auto-generated catch block
//	//			e.printStackTrace();
//	//		}
//	//		/*------------------------------------------------------------*/
//	//
//	//		
//	//		/*-----------ALMACENAMIENTO DE CERTIFICADO--------------------*/
//	//		
//	//		byte[] certificado_codificado = cert_x509.getEncoded();
//	//		
//	//		FileOutputStream fos;
//	//		try {
//	//			fos = new FileOutputStream("C:/cer/certificadoCA.cer");
//	//			fos.write(certificado_codificado);
//	//			fos.flush();
//	//			fos.close();
//	//
//	//		} catch (FileNotFoundException e) {
//	//			// TODO Auto-generated catch block
//	//			e.printStackTrace();
//	//		} catch (IOException e) {
//	//			// TODO Auto-generated catch block
//	//			e.printStackTrace();
//	//		}
//	//
//	//		/*------------------------------------------------------------*/
//	//
//	//		System.out.println("Finalizando .........\n");
//	//		boolean verificado= true;
//	//
//	//		try
//	//		{
//	//			cert_x509.verify(pubCA);
//	//		}
//	//		catch(Exception e)
//	//		{
//	//			System.out.println("ERROR VERIFICACION");
//	//			verificado = false;
//	//		}
//	//
//	//
//	//
//	//		System.out.println("Resultado verificacion:"+verificado);
//	//
//	//
//	//
//	//	}
//	//	}
//
//	//	
//	//}   
//
//
//
//
//
//
//
//
//
//
//	/*static void main(String[] args) throws CertificateEncodingException, IOException
//{
//
//	System.out.println("Elija una opción :\n");
//	System.out.println("Opción 1: Generar certificado CA");
//	System.out.println("Opción 2: Verificar certificado");
//	boolean salir = false;
//	while(!salir)
//	{
//		int resultado = System.in.read();
//
//
//		switch(resultado)
//		{
//
//			case 1:
//
//			generarCertificadoCA(new Date("26/05/2014"),1,"CA CONCURSO FOTOS");
//			break;
//
//			case 0:
//				salir = true;
//				break;
//
//
//		}
//
//	}
//	System.out.println("ADIÓS");
//
//
//}*/
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//	/*byte[] b = cert.getBytes();
//		//System.out.println("Longitud del certificado en bytes:"+b.length+"\n");
//		byte[] byteMessage = Base64.decodeBase64(b);
//		//System.out.println("mensaje recibido byte Length: "+byteMessage.length+"\n");
//
//		System.out.println("");
//
//
//			try {
//				FileOutputStream fos = new FileOutputStream("C:/cer2/certificateRA.cer");
//				fos.write(b);
//				fos.flush();
//				fos.close();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//
//
//
//		Certificate a = null;
//		try 
//		{
//			CertificateFactory cf = CertificateFactory.getInstance("X.509");
//
//			System.out.println("array input es "+b);
//			InputStream in = new ByteArrayInputStream(byteMessage);
//
//			a = cf.generateCertificate(in);
//
//
//		} 
//
//		catch (CertificateException e) 
//		{
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//		}
//
//
//		X509Certificate certx = (X509Certificate)a;
//
//
//		// Ahora que hemos obtenido el certificado, comprobamos su validez
//		try {
//			//comprobar_certificado(certx);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//
//    }
//
//	 */
//
//
