package controllers;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

public class PruebaCegado {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			prueba();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| UnsupportedEncodingException | SignatureException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static CipherParameters convertir_publica(PublicKey p)
	{
		// Convertimos la clave pública a un objeto RSAPublicKey para poder obtener su módulo y su exponente
		RSAPublicKey RSAp = (RSAPublicKey)p;
		
		//Creamos e instanciamos un objeto del tipo RSAKeyParameters con el módulo y exponente de la clave 
		// publica para poder generar el factor de cegado basándonos en ella a través de las librerías de BC
		RSAKeyParameters publica = new RSAKeyParameters(false,RSAp.getModulus(),RSAp.getPublicExponent());
		
		CipherParameters publicaC  = (CipherParameters)publica;


		return publicaC;
	}

	public static void prueba() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException, SignatureException, NoSuchProviderException
	{
		
		//Generate BOB key pair
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(1024);
		               
		KeyPair keyPair = kpg.genKeyPair();
		               
		//Get out bob's public and private keys
		PublicKey bobPubKey = keyPair.getPublic();
		PrivateKey bobPrivKey = keyPair.getPrivate();
		
		
		CipherParameters bobPubKeyC = convertir_publica(bobPubKey);
		
				
		
		
		/*
		* STEP 1: BLINDING MESSAGE
		* Alice blinds her message, m, with her key, b.
		* blind(m,b)
		*/
		               
		//Print out message in string and Hex
		byte[] aliceMessage = "This is the text for blinding".getBytes("UTF-8");
		System.out.println("Alice's Message(String): " + new String(aliceMessage));
		System.out.println("Alice's Message(Hex): " + HexBin.encode(aliceMessage));
		               
		//Generate secure random
		SecureRandom randomVal = SecureRandom.getInstance("SHA1PRNG");
		               
		//Use rsa key pair gen
		BigInteger bigInteger = new BigInteger("1001",16);
		RSAKeyPairGenerator rsaKeyPair = new RSAKeyPairGenerator();
		RSAKeyGenerationParameters rsaParams = new RSAKeyGenerationParameters(bigInteger,randomVal,1024,16);
		rsaKeyPair.init(rsaParams);
		AsymmetricCipherKeyPair rsaKeys = rsaKeyPair.generateKeyPair();
		             
		//Blinding factor
		RSABlindingFactorGenerator factor = new RSABlindingFactorGenerator();
		             
		//ok, got the rsa key parameters
		//RSAKeyParameters AlicepKey = (RSAKeyParameters) rsaKeys.getPublic();
		
		factor.init(bobPubKeyC);
		
		BigInteger fac = factor.generateBlindingFactor();
		       
		//Now, for blinding engine
		RSABlindingEngine eng = new RSABlindingEngine();
		
		//Now, for blinding parameters
		RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters)bobPubKeyC,fac);
			
		eng.init(true, params);
		             
		//blind data and store
		byte[] blindedData = null;
		try 
		{
			blindedData = eng.processBlock(aliceMessage, 0, aliceMessage.length);
		} 
		catch(Exception e)
		{
		System.err.println("Blinding failed. Error.");
		}
		             
		//Print out blinded messasge
		System.out.println("\nAlice's Blinded Message: " + HexBin.encode(blindedData));
		             
		/*
		* STEP 2: Sign blinded message
		* Alice has sent her blinded message to Bob so Bob can sign
		* Bob generates key pairs (private and public keys) and
		* signs Alice's blinded message with his private key, d.
		* sign(blind(m,b),d)
		*/
		             
		
		             
		//Signature engine
		Signature signatureEngine = Signature.getInstance("MD5withRSA");
		             
		//Initialise the signature to sign mode, with Bob's private key
		signatureEngine.initSign(bobPrivKey);
		             
		//Pass in blinded message
		signatureEngine.update(blindedData);
		                            
		//Obtain Bob's signature
		byte[] bobSignature = signatureEngine.sign();
		             
		//Print out Bob's signature
		System.out.println("\nBob's signature: " + HexBin.encode(bobSignature));
		                            
		/*
		* Now, Bob sends signed blinded message back to Alice.
		* Alice unblinds with the same key she used to blind the message
		* unblind(sign(blind(m,b),d),b)
		*/
		            
		//Unblind
		eng.init(false, params);
		             
		byte[] unBlindedData = null;
		try {
		unBlindedData = eng.processBlock(aliceMessage, 0, aliceMessage.length);
		} catch(Exception e){
		System.err.println("UnBlinding failed. Error!!! FUCK!");
		}
		             
		//Print out unblinded data: this should equate to bob's signature on
		//alice's message using his private key
		System.out.println("\nUnBlinded Message: " + HexBin.encode(unBlindedData));
		  
		try {
			System.out.println("descegado "+Hex.decodeHex(HexBin.encode(unBlindedData).toCharArray()));
		} catch (DecoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		* After unblinding, the result should be sign(m,d);
		* that is, it should be the Bob's signature on alices message, using his
		* private key.
		* 
		* (Remember, normally, it should have been alice sending the message,m, to bob
		* and bob signs it: sign(m,d), but the idea is to ensure that bob has no idea
		* what the message is).
		* 
		* Now, Alice must verify that the unblinded data is in fact bob's signature of her
		* message. To do this she needs Bob's public key, which he sent along with signture
		*/
		             
		//we already have a sig engine declared
		//Signature signatureEngine = Signature.getInstance("MD5WithRSA");
		             
		//Initialise the signature to verify mode
		signatureEngine.initVerify(bobPubKey);
		               
		//To verify, pass in alice's original message
		signatureEngine.update(aliceMessage);
		               
		boolean isVerified = false;
		try {
		//normally, you would pass in byte[] bobSignature
		//but remember, we want to verify the signature on alice's message,
		//which is sign(m,d), and this is in fact the unblinded data.
		isVerified = signatureEngine.verify(unBlindedData);
		} catch(SignatureException se) {
		System.out.println("Something went wrong...");
		}
		System.out.println("\nIs this bob's signature on Alice's message?: " + isVerified);
	}
	
}
