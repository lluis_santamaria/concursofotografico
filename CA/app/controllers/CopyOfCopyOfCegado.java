//package controllers;
//
//import java.math.BigInteger;
//import java.security.InvalidKeyException;
//import java.security.KeyFactory;
//import java.security.KeyPair;
//import java.security.NoSuchAlgorithmException;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.SecureRandom;
//import java.security.Signature;
//import java.security.SignatureException;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.RSAPrivateCrtKeySpec;
//import java.security.spec.RSAPublicKeySpec;
//
//import org.apache.commons.codec.DecoderException;
//import org.apache.commons.codec.binary.Hex;
//import org.bouncycastle.asn1.eac.RSAPublicKey;
//import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
//import org.bouncycastle.crypto.CipherParameters;
//import org.bouncycastle.crypto.digests.SHA1Digest;
//import org.bouncycastle.crypto.engines.RSABlindingEngine;
//import org.bouncycastle.crypto.engines.RSAEngine;
//import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
//import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
//import org.bouncycastle.crypto.params.RSABlindingParameters;
//import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
//import org.bouncycastle.crypto.params.RSAKeyParameters;
//import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
//import org.bouncycastle.crypto.signers.PSSSigner;
//
//import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
//
//public class CopyOfCopyOfCegado 
//
//{
//	
//
//		/*-----------Ejemplo para generar las claves RSA del cegado------*/
//	    public static AsymmetricCipherKeyPair generarClaves(int longitud_clave) {
//	     RSAKeyPairGenerator r = new RSAKeyPairGenerator();
//
//	     r.init(new RSAKeyGenerationParameters(new BigInteger("10001", 16), new SecureRandom(),
//	                longitud_clave, 80));
//
//	     AsymmetricCipherKeyPair claves = r.generateKeyPair();
//
//	   
//	     return claves;
//	    }
//
//	    public static BigInteger generateBlindingFactor(CipherParameters pubKey) {
//	        RSABlindingFactorGenerator gen = new RSABlindingFactorGenerator();
//
//	        gen.init(pubKey);
//
//	        return gen.generateBlindingFactor();
//	    }
//
//	    public static byte[] cegar(CipherParameters key, BigInteger factor, byte[] msg) 
//	    {
//	        RSABlindingEngine eng = new RSABlindingEngine();
//	       
//	      	        
//	        RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters)key, factor);
//	        PSSSigner blindSigner = new PSSSigner(eng, new SHA1Digest(), 15);
//	        blindSigner.init(true, params);
//
//	        blindSigner.update(msg, 0, msg.length);
//
//	        byte[] blinded = null;
//	        try {
//	            blinded = blindSigner.generateSignature();
//	        } catch (Exception ex) {
//	            ex.printStackTrace();
//	        }
//
//	        return blinded;
//	    }
//	    
//	    public static byte[] descegar(CipherParameters key, BigInteger factor, byte[] msg) {
//	        RSABlindingEngine eng = new RSABlindingEngine();
//
//	        RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters) key,factor);
//	        eng.init(false, params);
//
//	        return eng.processBlock(msg, 0, msg.length);
//	    }
//
//	    private static KeyPair convertirclavesCipher(RSAKeyParameters publicKey,RSAPrivateCrtKeyParameters privateKey) throws InvalidKeySpecException, NoSuchAlgorithmException
//	    {
//	    	PublicKey pubKey;
//			PrivateKey privKey;
//			
//			
//	        
//				pubKey = KeyFactory.getInstance("RSA").generatePublic(
//				        new RSAPublicKeySpec(publicKey.getModulus(), publicKey.getExponent()));
//			
//	        // and this one for the KeyStore
//	        privKey = KeyFactory.getInstance("RSA").generatePrivate(
//	                new RSAPrivateCrtKeySpec(publicKey.getModulus(), publicKey.getExponent(),
//	                        privateKey.getExponent(), privateKey.getP(), privateKey.getQ(), 
//	                        privateKey.getDP(), privateKey.getDQ(), privateKey.getQInv()));
//	        
//	        return new KeyPair(pubKey,privKey); 
//	    
//	    }
//	    public static byte[] sign(CipherParameters key, byte[] toSign) {
//	        SHA1Digest dig = new SHA1Digest();
//	        RSAEngine eng = new RSAEngine();
//
//	        PSSSigner signer = new PSSSigner(eng, dig, 15);
//	        signer.init(true, key);
//	        signer.update(toSign, 0, toSign.length);
//
//	        byte[] sig = null;
//
//	        try {
//	            sig = signer.generateSignature();
//	        } catch (Exception ex) {
//	            ex.printStackTrace();
//	        }
//
//	        return sig;
//	    }
//
//	    public static boolean verify(CipherParameters key, byte[] msg, byte[] sig) {
//	        PSSSigner signer = new PSSSigner(new RSAEngine(), new SHA1Digest(), 15);
//	        signer.init(false, key);
//
//	        signer.update(msg,0,msg.length);
//
//	        return signer.verifySignature(sig);
//	    }
//
//	    public static byte[] signBlinded(CipherParameters key, byte[] msg) {
//	        RSAEngine signer = new RSAEngine();
//	        signer.init(true, key);
//	        return signer.processBlock(msg, 0, msg.length);
//	    }
//
//	    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, SignatureException 
//	    {
//	        //AsymmetricCipherKeyPair bob_keyPair = CopyOfCopyOfCegado.generarClaves(1024);
//	        AsymmetricCipherKeyPair alice_keyPair = CopyOfCopyOfCegado.generarClaves(1024);
//	        //AsymmetricCipherKeyPair prova_keyPair = CopyOfCopyOfCegado.generarClaves(1024);
//	       
//	        KeyPair claves_alice = convertirclavesCipher((RSAKeyParameters)alice_keyPair.getPublic(), (RSAPrivateCrtKeyParameters)alice_keyPair.getPrivate());
//	        //KeyPair claves_bob = convertirclavesCipher((RSAKeyParameters)bob_keyPair.getPublic(), (RSAPrivateCrtKeyParameters)bob_keyPair.getPrivate());
//	        byte[] msg=null;
//	        byte[] AliceSignature=null;
//	        BigInteger blindingFactor= null;
//	        byte[] sigByAlice=null;
//	        byte[] unblindedSigByAlice = null;
//	        try 
//	        {
//	             msg= "OK".getBytes("UTF-8");
//
//	            //----------- Bob: Generating blinding factor based on Alice's public key -----------//
//	             blindingFactor = CopyOfCopyOfCegado.generateBlindingFactor(alice_keyPair.getPublic());
//
//	            //----------------- Bob: Blinding message with Alice's public key -----------------//
//	            byte[] blinded_msg = CopyOfCopyOfCegado.cegar(alice_keyPair.getPublic(), blindingFactor, msg);
//
////	            //------------- Bob: Signing blinded message with Bob's private key -------------//
////	            byte[] sig = CopyOfCopyOfCegado.sign(bob_keyPair.getPrivate(), blinded_msg);
////
////	            //------------- Alice: Verifying Bob's signature -------------//
////	            if (CopyOfCopyOfCegado.verify(bob_keyPair.getPublic(), blinded_msg, sig)) {
//
//	            
//	            
//	   /*****************************************************************************************************/
//	            
//	            
//	            
//	    		            
//	    		
//	    		 //---------- Alice: Signing blinded message with Alice's private key ----------//
//                
//	            sigByAlice =CopyOfCopyOfCegado.signBlinded(alice_keyPair.getPrivate(), blinded_msg);
//                
//                Signature signatureEngine = Signature.getInstance("SHA1withRSA");
//	             
//	    		//Initialise the signature to sign mode, with Bob's private key
//	    		signatureEngine.initSign(claves_alice.getPrivate());
//	    		             
//	    		//Pass in blinded message
//	    		signatureEngine.update(blinded_msg);
//	    		                            
//	    		//Obtain Bob's signature
//	    		AliceSignature = signatureEngine.sign();
//                                 
//                
//                
//                
//
//                //------------------- Bob: Unblinding Alice's signature -------------------//
//                unblindedSigByAlice = CopyOfCopyOfCegado.descegar(alice_keyPair.getPublic(), blindingFactor, sigByAlice);
//
//                //---------------- Bob: Verifying Alice's unblinded signature ----------------//
//                System.out.println(CopyOfCopyOfCegado.verify(alice_keyPair.getPublic(), msg,
//                        unblindedSigByAlice));
//                
//                String msg2 = new String(unblindedSigByAlice);
//               
//               // Now Bob has Alice's signature for the original message
//
//	        }
//	        catch (Exception e) 
//	        {
//	            e.printStackTrace();
//	        }
//	        
//	        
//	        System.out.println("Comprobamos firma");
//	        
//	        Signature a = Signature.getInstance("SHA1withRSA");
//	        
//	        a.initVerify(claves_alice.getPublic());
//	        a.update(msg);
//	        
//	        //byte[] descegada = descegar(alice_keyPair.getPublic(),blindingFactor,sigByAlice);
//	        
//	       // boolean resultado = verify(alice_);
//	        
//	        System.out.println();
//	        
//	       // System.out.println("Resultado = "+resultado);
//	    
//	    }
//
//	}            
//	            
//	                    
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
//	            
////	               
//
//
//
