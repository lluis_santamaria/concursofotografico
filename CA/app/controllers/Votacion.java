package controllers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import play.mvc.Controller;

/**
 * Clase que contiene el codigo que se insertará en el Applet de la página
 * web para el proceso de votación
 * 
 * @author Lluís Santamaria Solà
 *
 */
public class Votacion extends Controller
{

	/**
	 * 
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception 
	{
		//Pruebas
		
		start_votacion("lluis",3);

	}
	
	/**
	 * Rutas de conexión entre servidores
	 */
	static byte[] certificado_user;
	final static String ruta_server_concurso = "http://192.168.1.135:9990/";
	final static String ruta_CA = "http://192.168.1.135:7777/CertificationAuthority/request_publica";
	final static String ruta_CA_claves = "http://192.168.1.135:7777/CertificationAuthority/genera_claves";
	final static String ruta_urna = "http://192.168.1.135:6666/Application/";
	
	/**
	 * Rutas de guardado de certificados y votos
	 * 
	 * */

	final static String ruta_guardado_certificado = "C:/files_voto/cert.xml";
	final static String ruta_guardado_certificado_firmado = "C:/files_voto/cert_firmado.xml";
	final static String ruta_guardado_voto_firmado = "C:/files_voto/voto_firmado.xml";
	final static String ruta_guardado_voto = "C:/files_voto/voto.xml";
	
	
	/**
	 * Claves globales de CA. Obtenidas mediante request al servidor CA
	 */
	
	static PublicKey clavePublicaCA;
	static CipherParameters clavePublicaCACipher;
	
	/**
	 * Se instancia cuando cuando obtenemos la verificación de la firma de la CA
	 */
	static byte[] certValido; 
	
	
	/**
	 * -------CLAVES USER-----------
	 * 
	 * */
	
	static KeyPair claves_user;
	static BigInteger FactorCegado;
	static int foto_votada;
	static String pseudonimo;
	
	
	
	/**
	 * Método que inicia el proceso de votación.
	 * @param pseudonim
	 * @param voto
	 * @throws Exception
	 */
	private static void start_votacion(String pseudonim,int voto) throws Exception
	{
		
		pseudonimo = pseudonim;
		foto_votada = voto;
		certificado_user = null;

		GeneraClavesCA();
		
		System.out.println("------EMPIEZA VOTACION-----\n");

		claves_user = generacion_claves_user();

		System.out.print("Construyendo certificado usuario\n");

		certificado_user = construir_certificado(pseudonimo,claves_user.getPublic());


		PublicKey clavePublicaCA2 = obtener_publica_CA();

		CipherParameters publicaC = convertir_publica(clavePublicaCA2);

		clavePublicaCA = clavePublicaCA2;
		
		clavePublicaCACipher= publicaC;
		
		/*----------PROCESO DE CEGADO DEL CERTIFICADO DEL USUARIO-------*/
		//Hay que mencionar que se entra en un bucle para conseguir que las claves no contengan el 
		//símbolo '+' al codificarlas ya que invalidan la firma por motivos desconocidos
		
		boolean encontrado = false;
		byte[] certCegadoStr=null;
		while(!encontrado)
		{	
			byte[] certCegado = cegar(certificado_user,publicaC);
			certCegadoStr= Base64.encodeBase64(certCegado);
			String certCegados = new String(certCegadoStr);
			
			if(!certCegados.contains("+"))
			{
				encontrado = true;
			}
		}
		
		//Enviamos al servidor del concurso el cert cegado

		user_sendPostCertCegadoaConcurso(new String(certCegadoStr,"UTF-8"));

		
		
	}
	
	private static void user_sendPostCertCegadoaConcurso(String certCegado) throws Exception 
	{
			
		//------PARaMETROS A ENVIAR----------------//
			
		
		System.out.println("Enviamos cert cegado = .................\n"+certCegado);
		String urlParameters = "certCegado="+certCegado;
		
			
		//--------URL---------------//
		String url = ruta_server_concurso+"Security/recibir_certCegado_concurso";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		//CABECERA
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'POST' a la  URL : " + url);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		//print result
		System.out.println("USUARIO : CERT GEGADO FIRMADO :------"+response.toString());
	
		byte[] certCegadoFirmado = Base64.decodeBase64(response.toString());
		
		byte[] certDesCegadoFirmado = Cegado.descegar(clavePublicaCACipher, FactorCegado, certCegadoFirmado);
		
		boolean verificacion_firma = Cegado.verify(clavePublicaCACipher, certificado_user, certDesCegadoFirmado);
		
		System.out.println("\n-----------------------USER VERIFICACION : "+verificacion_firma+"-----------------\n");
		
		certValido = certDesCegadoFirmado;
		AutenticacionUrna();
	}
	
	/**
	 * Creación de voto XML
	 * 
	 * @param foto_votada
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws SignatureException
	 */
	static void crear_voto(int foto_votada) throws ParserConfigurationException, SAXException, 
	IOException, TransformerException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException
	{

		//Instanciamos las clases para crear el voto XML



		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 


		//Creamos el fichero xml con el campo voto donde irá su valor

		Document doc = db.newDocument();


		//Asignamos la votación elegida
		Element rootElement = doc.createElement("Voto");
		doc.appendChild(rootElement);
		
				
		Element pseudonim = doc.createElement("Pseudonimo");
		
		Element fotoVotada = doc.createElement("FotoVotada");
	

		fotoVotada.appendChild(doc.createTextNode(Integer.toString(foto_votada)));
		pseudonim.appendChild(doc.createTextNode(pseudonimo));
		
		rootElement.appendChild(pseudonim);
		rootElement.appendChild(fotoVotada);
		
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(ruta_guardado_voto));

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.transform(source, result);

		firmar_voto(doc,"voto.xml",claves_user.getPrivate());




	}
	
	/**
	 * Firmamos el Voto que hemos generado con nuestra clave privada
	 * @param d
	 * @param nombre
	 * @param priv
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws InvalidKeyException
	 * @throws SignatureException
	 * @throws IOException
	 * @throws TransformerException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	
	static void firmar_voto(Document d,String nombre,PrivateKey priv) throws NoSuchAlgorithmException, NoSuchProviderException, 
	InvalidKeyException, SignatureException, IOException, TransformerException, 
	ParserConfigurationException, SAXException
	{
		Security.addProvider(new BouncyCastleProvider());
		Signature sig = Signature.getInstance("SHA1withRSA", "BC");
		sig.initSign(priv);

		FileInputStream fis = new FileInputStream("C:/files_voto/"+nombre);
		BufferedInputStream bufin = new BufferedInputStream(fis);
		byte[] buffer = new byte[1024];
		int len;
		while ((len = bufin.read(buffer)) >= 0) {
			sig.update(buffer, 0, len);
		};
		bufin.close();

		byte[] firma = sig.sign();

		crear_voto_firmado(firma);


	}
	/**
	 * Creamos el voto firmado 
	 * 
	 * @param firma
	 * @throws TransformerException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws SignatureException
	 */
	
	static void crear_voto_firmado( byte[] firma) throws TransformerException, 
	ParserConfigurationException, SAXException, IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException
	{

		File fXmlFile = new File(ruta_guardado_voto);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbFactory.newDocumentBuilder();

		Document doc = db.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("Voto");
		Element voto = (Element)nList.item(0);
		Element firmaElement = doc.createElement("Firma");
		String firma_coded = Base64.encodeBase64String(firma.toString().getBytes());
		firmaElement.appendChild(doc.createTextNode(firma_coded));
		Attr attr2 = doc.createAttribute("id");
		attr2.setValue("Firma");
		firmaElement.setAttributeNode(attr2);	
		voto.appendChild(firmaElement);
		DOMSource source = new DOMSource(doc);
		
		//Guardamos el voto firmado
		StreamResult result = new StreamResult(new File(ruta_guardado_voto_firmado));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.transform(source, result);
			
		
		enviar_voto_urna();

	}
	
	/**
	 * En
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws SignatureException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws TransformerException
	 */
	
	private static void enviar_voto_urna() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException, ParserConfigurationException, SAXException, TransformerException
	{
		FileInputStream fileInputStream=null;
		 
        File file = new File(ruta_guardado_voto_firmado);
 
        byte[] bFile = new byte[(int) file.length()];
 
        try {
            //convert file into array of bytes
	    fileInputStream = new FileInputStream(file);
	    fileInputStream.read(bFile);
	    fileInputStream.close();
 
	    for (int i = 0; i < bFile.length; i++) {
	       	System.out.print((char)bFile[i]);
            }
 
	    System.out.println("Done");
        }catch(Exception e){
        	e.printStackTrace();
        }
        
       
        
        String votocoded = Base64.encodeBase64String(bFile);
        
        
        
        System.out.println("VOTO ABANS ENVIO \n"+votocoded);
        
        sendVoto(new String(bFile));
        
		
	}
	private static void sendVoto(String voto) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, ParserConfigurationException, SAXException, TransformerException
	{
		//****????*****------PARaMETROS A ENVIAR----------------//
		
		String urlParameters = "votoCoded="+voto;
			
		//--------URL---------------//
		String url = ruta_urna+"recibir_voto";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		//CABECERA
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
				
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		//String clave = response.toString().replace(" ", "+");
		//print result
		System.out.println("RESPUESTA URNA ---------------------------"+response.toString());
	
		//crear_voto(3);
			
		
	}
	
	
	
	private static void AutenticacionUrna() throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, ParserConfigurationException, SAXException, TransformerException
	{
		byte[] certCoded = Base64.encodeBase64(certValido);
		sendCertificadoUrna(new String(certCoded), certificado_user);
	}
	
	private static void sendCertificadoUrna(String certF,byte[] certNF) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, ParserConfigurationException, SAXException, TransformerException
	{
		//****????*****------PARaMETROS A ENVIAR----------------//
		
		String urlParameters = "certFirmadoCoded="+certF+"&certOrig="+new String(Base64.encodeBase64(certNF));
			
		//--------URL---------------//
		String url = ruta_urna+"verificar_firma";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		//CABECERA
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
				
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		String clave = response.toString().replace(" ", "+");
		//print result
		System.out.println("RESPUESTA URNA ---------------------------"+response.toString());
	
		crear_voto(3);
			
		
	}
	private static void GeneraClavesCA() throws IOException
	{
	
		
		//--------URL---------------//
		String url = ruta_CA_claves;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		//CABECERA
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		//String clave = response.toString().replace(" ", "+");
		//print result
		//System.out.println("????????PUBLICA LONG : "+clave.length()+"\nRESPUESTA---------------------------"+response.toString());
	
			
		
		
	}
	private static CipherParameters convertir_publica(PublicKey p)
	{
		// Convertimos la clave pública a un objeto RSAPublicKey para poder obtener su módulo y su exponente
		RSAPublicKey RSAp = (RSAPublicKey)p;

		//Creamos e instanciamos un objeto del tipo RSAKeyParameters con el módulo y exponente de la clave 
		// publica para poder generar el factor de cegado basándonos en ella a través de las librerías de BC
		RSAKeyParameters publica = new RSAKeyParameters(false,RSAp.getModulus(),RSAp.getPublicExponent());

		CipherParameters publicaC  = (CipherParameters)publica;


		return publicaC;
	}
	private static KeyPair generacion_claves_user() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
	{

		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");


		RSAKeyPairGenerator gen = new RSAKeyPairGenerator();
		gen.init(new RSAKeyGenerationParameters(BigInteger.valueOf(3), sr, 1024, 80));
		AsymmetricCipherKeyPair keypair = gen.generateKeyPair();
		RSAKeyParameters publicKey = (RSAKeyParameters) keypair.getPublic();
		RSAPrivateCrtKeyParameters privateKey = (RSAPrivateCrtKeyParameters) keypair.getPrivate();

		/* Definimos una estructura de la clave pública del tipo RSA, que estará constituida por el módulo común con
           la clave privada, y un exponente característico y unívoco propio de la pública */

		RSAPublicKeyStructure pkStruct = new RSAPublicKeyStructure(publicKey.getModulus(), publicKey.getExponent());

		/* Hasta ahora hemos trabajado con "sintaxis/terminología/objetos" de las librerías BouncyCastle. Para poder trabajar
		    con las librerías de JCE, necesitamos hacer una conversión en las claves */

		// formato JCE 
		PublicKey pubKey;
		PrivateKey privKey;


		pubKey = KeyFactory.getInstance("RSA").generatePublic(
				new RSAPublicKeySpec(publicKey.getModulus(), publicKey.getExponent()));
		// and this one for the KeyStore
		privKey = KeyFactory.getInstance("RSA").generatePrivate(
				new RSAPrivateCrtKeySpec(publicKey.getModulus(), publicKey.getExponent(),
						privateKey.getExponent(), privateKey.getP(), privateKey.getQ(), 
						privateKey.getDP(), privateKey.getDQ(), privateKey.getQInv()));

		return new KeyPair(pubKey,privKey); 
	}

	private static byte[] construir_certificado(String pseudo, PublicKey pub) throws ParserConfigurationException, TransformerException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 


		//Creamos el fichero xml con el pseudonimo del votante y la clave pública

		Document doc = db.newDocument();


		//Asignamos la votación elegida
		Element rootElement = doc.createElement("Certificado");
		doc.appendChild(rootElement);

		Element PseudonimElement = doc.createElement("Pseudonimo");
		PseudonimElement.appendChild(doc.createTextNode("\n"));
		PseudonimElement.appendChild(doc.createTextNode(pseudo));
		PseudonimElement.appendChild(doc.createTextNode("\n"));
		rootElement.appendChild(PseudonimElement);

		Element PublicKeyElement = doc.createElement("PublicKey");
		PublicKeyElement.appendChild(doc.createTextNode("\n"));
		PublicKeyElement.appendChild(doc.createTextNode(Base64.encodeBase64String(pub.getEncoded())));

		Attr attr = doc.createAttribute("id");
		attr.setValue("ClavePublica");
		PublicKeyElement.setAttributeNode(attr);
		rootElement.appendChild(PublicKeyElement);

		//rootElement.appendChild(doc.createTextNode(Integer.toString(3)));

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer= transformerFactory.newTransformer();

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(ruta_guardado_certificado));

		//StreamResult result =  new StreamResult(System.out);



		transformer.transform(source, result);


		ByteArrayOutputStream bos=new ByteArrayOutputStream();

		StreamResult result2 =new StreamResult(bos);
		transformer.transform(source, result2);

		byte [] certUserNoFirmado =bos.toByteArray();

		return certUserNoFirmado;
	}
	

	private static PublicKey obtener_publica_CA() throws Exception
	{

		String publica = requestPublicaCA();
		
		System.out.println("publica que recibe user  "+publica);
		/*FileOutputStream fos = new FileOutputStream("C:/cer/publicaObtenidaUser.cer");
			
		fos.write(publica.getBytes());
		fos.flush();
		fos.close();*/
			
		byte [] publicadec = Base64.decodeBase64(publica);
		/*FileInputStream keyfis = new FileInputStream(ruta_guardado_clave_CA);


		byte[] claveCodificada = new byte[keyfis.available()]; 

		//byte[] clavedescodificada = Base64.decodeBase64(claveCodificada);
		keyfis.read(claveCodificada);
		keyfis.close();*/

		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicadec);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    PublicKey pubKey =  keyFactory.generatePublic(pubKeySpec);

		System.out.println("PUBLICA OBTENIDA USER: "+publica);
		
		return pubKey;
	}
	private static String requestPublicaCA() throws Exception 
	{
			
		//****????*****------PARaMETROS A ENVIAR----------------//
				
		//String urlParameters = certCegado;
			
		//--------URL---------------//
		String url = ruta_CA;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		//CABECERA
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'GET' a la  URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		String clave = response.toString().replace(" ", "+");
		//print result
		System.out.println("????????PUBLICA LONG : "+clave.length()+"\nRESPUESTA---------------------------"+response.toString());
	
		
		return response.toString();
		
	}
	
	private static byte[] cegar(byte[] cert_a_cegar, CipherParameters c) throws UnsupportedEncodingException
	{
		System.out.println("Cegando...");

		byte[] CertFirmado= null;
		byte[] CertFirmadoDescegado= null;
		byte[] CertCegado = null;

		

		// cert_a_cegar = "OK".getBytes("UTF-8");

		try 
		{


			//----------- Usuario: Genera el factor de cegado basado en la clave pública del validador -----------//
			FactorCegado = Cegado.generateBlindingFactor(clavePublicaCACipher);

			//----------------- Usuario: ciega el mensaje con el factor de cegado -----------------//
			CertCegado = Cegado.cegar(clavePublicaCACipher, FactorCegado, cert_a_cegar);

			//		            //-------------Usuario: Firma el documento OPCIONAL. PARA FIRMAR EL CONTENIDO Y QUE CA VERIFIQUE QUE SOMOS NOSOTROS -------------//
			//		            byte[] sig = Cegado.sign(privatekeyuser blinded_cert_a_cegar);
			//
			//		           

		} 
		catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Cegado OK");
		return CertCegado;

	}
	
	
	

	
	
	

}
