package controllers;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;


import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;

public class CA 
{

	/**
	 * @param args
	 * @throws SignatureException 
	 * @throws NoSuchProviderException 
	 * @throws InvalidKeyException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 * @throws CertificateEncodingException 
	 */
	public static void main(String[] args) throws InvalidKeyException, NoSuchProviderException, 
	SignatureException, NoSuchAlgorithmException, InvalidKeySpecException, CertificateEncodingException 
	{
		// TODO Auto-generated method stub

				generar_clavesCA(1024);
		//		
		//		genCertCA();

		byte[] prova = "OK".getBytes();

		byte[] prova1 = cegar(prova);



	}


	static PublicKey pubKey;
	static PrivateKey privKey;

	static CipherParameters pubKeyCipher;
	static CipherParameters privKeyCipher;

	private static void generar_clavesCA(int longitud) throws NoSuchAlgorithmException, NoSuchProviderException,
	InvalidKeySpecException
	{

		// Obtenemos el numero aleatorio de sun systems
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");

		// Creamos un nuevo objeto RSAKeypPairGenerator para generar un par de claves de RSA
		RSAKeyPairGenerator gen = new RSAKeyPairGenerator();

		// Inicializamos el generador de claves
		gen.init(new RSAKeyGenerationParameters(BigInteger.valueOf(3), sr, longitud, 80));

		// Generamos las claves
		AsymmetricCipherKeyPair keypair = gen.generateKeyPair();

		// Obtenemos las claves pública y privada
		RSAKeyParameters publicKey = (RSAKeyParameters) keypair.getPublic();
		RSAPrivateCrtKeyParameters privateKey = (RSAPrivateCrtKeyParameters) keypair.getPrivate();

		/* Definimos una estructura de la clave pública del tipo RSA, que estará constituida por el módulo común con
		la clave privada, y un exponente característico y unívoco propio de la pública */
		RSAPublicKeyStructure pkStruct = new RSAPublicKeyStructure(publicKey.getModulus(), publicKey.getExponent());

		/* Hasta ahora hemos trabajado con "sintaxis/terminología/objetos" de las librerías BouncyCastle. Para poder trabajar
			con las librerías de JCE, necesitamos hacer una conversión en las claves */

		pubKey = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(publicKey.getModulus(), publicKey.getExponent()));

		privKey = KeyFactory.getInstance("RSA").generatePrivate(
				new RSAPrivateCrtKeySpec(publicKey.getModulus(), publicKey.getExponent(),
						privateKey.getExponent(), privateKey.getP(), privateKey.getQ(), 
						privateKey.getDP(), privateKey.getDQ(), privateKey.getQInv()));


		/* En este punto tenemos 2 pares de claves. Uno en formato de BC, y otro en formato Java. Con el primero implementaremos la firma 
			ciega y con el segundo generaremos el certificado */
	}
	
	private static byte[] cegar(byte[] cert_a_cegar)
	{

		
		byte[] certFirmadoDescegado=null;
		byte[] CertFirmadoPorCA= null;
		byte[] CertFirmadoDescegado= null;
		
		try
		{


			Cegado c= new Cegado();

			//----------- Usuario: Genera el factor de cegado basado en la clave pública del validador -----------//
			/**BigInteger FactorCegado = c.generateBlindingFactor(clavesCA.getPublic());**/
			BigInteger FactorCegado = c.generateBlindingFactor(pubKeyCipher);
			//----------------- Usuario: ciega el mensaje con el factor de cegado -----------------//
			/**byte[] CertCegado =c.cegar(clavesCA.getPublic(), FactorCegado, cert_a_cegar);*/
			byte[] CertCegado =c.cegar(pubKeyCipher, FactorCegado, cert_a_cegar);

			/*----------OPCIONAL--------------*/

			/*
			//------------- Usuario: Firma el mensaje con su clave privada -------------
			byte[] sig = c.sign(clavesUser.getPrivate(), blinded_msg);

			------------- CA: Verifica la firma del usuario -------------
			if (c.verify(clavesUser.getPublic(), blinded_msg, sig)) 
			{*/

			/*---------------------------------*/


			//---------- CA: Firma el certificado con su clave privada ----------//
			/**CertFirmadoPorCA =c.signBlinded(clavesCA.getPrivate(), CertCegado);*/
			CertFirmadoPorCA =c.signBlinded(privKeyCipher, CertCegado);

			//------------------- Usuario: Desciega la firma -------------------//
			/**CertFirmadoDescegado =c.descegar(clavesCA.getPublic(), FactorCegado, CertFirmadoPorCA);*/
			CertFirmadoDescegado =c.descegar(pubKeyCipher, FactorCegado, CertFirmadoPorCA);
			
			
			//---------------- Usuario: Verifica la firma descegada de  CA ----------------//
			/**System.out.println(c.verify(clavesCA.getPublic(), cert_a_cegar,certFirmadoDescegado));*/
			System.out.println(" ------RESULTAT: "+c.verify(pubKeyCipher, cert_a_cegar,certFirmadoDescegado));
			String msg2 = new String(certFirmadoDescegado);

			//Ahora el usuario tiene la firma del contenido inicial
			//}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return CertFirmadoDescegado;

	}
	



}
