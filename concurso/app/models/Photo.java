package models;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import controllers.Application;

import play.db.jpa.Model;

@Entity
public class Photo extends Model
{
	
	 int id_photo;
	 private String coordenadas;
	 public String Path;
	 public String PathSIEXIF;
	 int shownumber;
	 private String modelo;
	 private String exposeture;
	 private String date;
	 private String focal;
	 private String flash;
	 private String lens;
	 private double altitud;
	 private double latitud;
	
	
	
	public Photo(int id_photo, String coordenadas, Image picture) {
		super();
		this.id_photo = id_photo;
		this.coordenadas = coordenadas;
		//this.picture = picture;
	}
	public List<Photo> percent_show_algorithm()
	{
		//pecent_show_algorithm funcion: VAMOS A CONTRUIR UNA LISTA CON EL 25% DE LAS FOTOS MENOS ENSEÑADAS DE LA DB
		System.out.println("pecent_show_algorithm funcion: VAMOS A CONTRUIR UNA LISTA CON EL 25% DE LAS FOTOS MENOS ENSEÑADAS DE LA DB");
		
		int i=0;
		int j=0;
		int k=0;
		int z =0;
		int z2=0;
		double percent;
		int percentINT;
		List<Photo> L = Photo.findAll();
		Photo[] photos = new Photo[L.size()];
		percent=L.size()*(0.25); //25 porciento
		percentINT = (int)percent;
		Photo p = new Photo();	
		List<Photo> L25percent = new ArrayList<>();
		
		
		while(z<L.size())
		{
			//metemos en un vector la lista para preparar el algoritmo de ordenacion de shownumbers
			photos[z]=L.get(z);	
			z++;
		}
		
		z2=0;
		while (z2<photos.length)
		{
			//ordenamos un vector de photos segun el numero de veces mostrado
			
			i=0;
			while (i<photos.length-1)
			{			
				if(photos[i].shownumber>photos[i+1].shownumber)
				{
					p=photos[i];
					photos[i]=photos[i+1];
					photos[i+1]=p;			
				}
				i++;		
			}
			z2++;
		}
		
		j=0;
		while(j<percentINT)
		{
			//contruimos una lista con el 25 porciento de las fotos menos vistas
			L25percent.add(photos[j]);
			j++;
		}
		
		k=0;
		while(k<L25percent.size())
		{
			//Actualizamos el numero de veces muestradas las fotos
			Photo pUpdate = Photo.find("byId_photo", L25percent.get(k).getId_photo()).first();
			pUpdate.setShownumber(pUpdate.getShownumber()+1);
			pUpdate.save();
			k++;
		}
		return L25percent;//enviamos la lista con el 25% de fotos menos muestradas
	}
	public String getPath() {
		return Path;
		
	}

	public void setPath(String path) {
		Path = path;
	}

	public Photo() 	{
		// TODO Auto-generated constructor stub
	}

	public int getid_photo() {
		return id_photo;
	}

	public int getId_photo() {
		return id_photo;
	}
	public void setId_photo(int id_photo) {
		this.id_photo = id_photo;
	}
	public int getShownumber() {
		return shownumber;
	}
	public void setShownumber(int shownumber) {
		this.shownumber = shownumber;
	}
	public void setid_photo(int id_photo) {
		this.id_photo = id_photo;
	}

	public String getCoordenadas() {
		return this.coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}
	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getExposeture() {
		return this.exposeture;
	}

	public void setExposeture(String exposeture) {
		this.exposeture = exposeture;
	}
	public String getDate() {
		return this.date;
	}
	public double getAltitud() {
		return altitud;
	}
	public void setAltitud(double altitud) {
		this.altitud = altitud;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFocal() {
		return this.focal;
	}

	public void setFocal(String focal) {
		this.focal = focal;
	}
	public String getFlash() {
		return this.flash;
	}

	public void setFlash(String flash) {
		this.flash = flash;
	}
	public String getLens() {
		return this.lens;
	}

	public void setLens(String lens) {
		this.lens = lens;
	}

	public double latitud(){
		   
		   String string = this.getCoordenadas();
		   String[] parts = string.split(" ");
		   String grados = parts[5]; 
		   double g=Double.parseDouble(grados);
		   
		   String minutos = parts[7];
		   String[] min = minutos.split("'");
		   String minut = min[0];
		   double m=Double.parseDouble(minut);
		   //System.out.println(m/60);
		   
		   String segundos = parts[8];
		   char corte=(char)(34);
		   String cortef=String.valueOf(corte);
		   String[] seg = segundos.split(cortef);
		   String sec = seg[0];
		   System.out.println(sec);
		   double s=Double.parseDouble(sec);
		   //System.out.println(s/3600);
		   
		   System.out.println(g+m/60+s/3600);
		   double total=g+m/60+s/3600;
		   
		return total;
	}

	public double altitud(){
		//System.out.println(pics.getCoordenadas());
		   
		   String string = this.getCoordenadas();
		   String[] parts = string.split(" ");
		   String grados = parts[0]; 
		   double g=Double.parseDouble(grados);
		   
		   String minutos = parts[2];
		   String[] min = minutos.split("'");
		   String minut = min[0];
		   double m=Double.parseDouble(minut);
		   //System.out.println(m/60);
		   
		   String segundos = parts[3];
		   char corte=(char)(34);
		   String cortef=String.valueOf(corte);
		   String[] seg = segundos.split(cortef);
		   String sec = seg[0];
		   System.out.println(sec);
		   double s=Double.parseDouble(sec);
		   //System.out.println(s/3600);
		   
		   System.out.println(g+m/60+s/3600);
		   double total=g+m/60+s/3600;
		   
		return total;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		

	}

}
