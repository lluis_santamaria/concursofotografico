package models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;





public class ExifToolExtractor {
	  private String exifToolApp;
	  public ExifToolExtractor(String exifToolApp) {
	    this.exifToolApp = exifToolApp;
	  }
	  public ExifInfo getExifInfo(String image) throws IOException, InterruptedException {
	    ProcessBuilder processBuilder = new ProcessBuilder(exifToolApp, image);
	    Process process = processBuilder.start();
	    BufferedReader stdInput = new BufferedReader(new
	        InputStreamReader(process.getInputStream()));
	    BufferedReader stdError = new BufferedReader(new
	        InputStreamReader(process.getErrorStream()));
	    String line;
	    ExifInfo exifInfo = new ExifInfo();
	    while ((line = stdInput.readLine()) != null) {
	      exifInfo.parseLine(line);
	      System.out.println(line);
	    }
	    while ((line = stdError.readLine()) != null) {
	    }
	    process.waitFor();
	    return exifInfo;
	  }


	public static ExifInfo main( String paths) throws Exception {
	   //ExifToolExtractor exifToolExtractor = new ExifToolExtractor("D:/UPC/4A/cities2/ProyectoConcurso/trunk/ProjecteServidor/concurso/public/exiftool/exiftool.exe");
		ExifToolExtractor exifToolExtractor = new ExifToolExtractor("C:/Users/marti/Documents/Cities/trunk/ProjecteServidor/concurso/public/exiftool/exiftool.exe");
	    ExifInfo exifInfo = exifToolExtractor.getExifInfo(paths);
	    
	    
	    System.out.println(exifInfo);
	    return exifInfo;
	  }
	

}