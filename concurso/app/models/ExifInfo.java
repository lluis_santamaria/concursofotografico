package models;


import java.util.HashMap;
import java.util.Map;
import play.db.jpa.Model;


public class ExifInfo {
  private Map<String, String> exifMap = new HashMap<String, String>();
  public static final String COMPANY = "Make";
  public static final String MODEL = "Camera Model Name";
  public static final String EXPOSURE_TIME = "Exposure Time";
  public static final String F_NUMBER = "F Number";
  public static final String ISO = "ISO";
  public static final String EXPOSURE_COMPENSATION = "Exposure Compensation";
  public static final String FOCAL_LENGTH = "Focal Length";
  public static final String EXPOSURE_PROGRAM = "Exposure Program";
  public static final String DATE_TAKEN = "Create Date";
  public static final String METERING_MODE = "Metering Mode";
  public static final String FLASH = "Flash";
  public static final String GPS_Position = "GPS Position";
  public static final String FLASH_EXPOSURE_COMPENSATION = "Flash Exposure Compensation";
  public static final String LENS = "Lens ID";
  public void parseLine(String line) {
    int pos = line.indexOf(":");
    if (pos != -1) {
      exifMap.put(line.substring(0, pos).trim(), line.substring(pos + 1).trim());
    }
  }
  public String getCompany() {
    return exifMap.get(COMPANY);
  }
  public String getModel() {
    return exifMap.get(MODEL);
  }
  public String getExposureTime() {
    return exifMap.get(EXPOSURE_TIME);
  }
  public String getFNumber() {
    return exifMap.get(F_NUMBER);
  }
  public String getISO() {
    return exifMap.get(ISO);
  }
  public String getExposureCompensation() {
    return exifMap.get(EXPOSURE_COMPENSATION);
  }
  public String getFocalLength() {
    return exifMap.get(FOCAL_LENGTH);
  }
  public String getExposureProgram() {
    return exifMap.get(EXPOSURE_PROGRAM);
  }
  public String getDateTaken() {
    return exifMap.get(DATE_TAKEN);
  }
  public String getMeteringMode() {
    return exifMap.get(METERING_MODE);
  }
  public String getFlash() {
    return exifMap.get(FLASH);
  }
  public String getGPS() {
	    return exifMap.get(GPS_Position);
	  }
  public String getFlashExposureCompensation() {
    return exifMap.get(FLASH_EXPOSURE_COMPENSATION);
  }
  public String getLens()
  {
    return exifMap.get(LENS);
  }
  public String toString() {
    return 
        "Model: " + getModel() + "\n" +
        "Exposure Time: " + getExposureTime() + "\n" +
        "Date Taken: " + getDateTaken() + "\n" +
        "Focal Length: " + getFocalLength() + "\n" +
        "Flash: " + getFlash() + "\n" +
        "Lens: " + getLens()+ "\n" +
        "GPS Position: " + getGPS() + "\n" ;
  }
}

//return "Company: " + getCompany() + "\n" +
//"Model: " + getModel() + "\n" +
//"Exposure Time: " + getExposureTime() + "\n" +
//"Date Taken: " + getDateTaken() + "\n" +
//"F-Number: " + getFNumber() + "\n" +
//"ISO: " + getISO() + "\n" +
//"Exposure Compensation: " + getExposureCompensation() + "\n" +
//"Metering Mode: " + getMeteringMode() + "\n" +
//"Focal Length: " + getFocalLength() + "\n" +
//"Exposure Program: " + getExposureProgram() + "\n" +
//"Flash: " + getExposureProgram() + "\n" +
//"GPS Position: " + getGPS() + "\n" +
//"Flash Exposure Compensation: " + getExposureProgram() + "\n" +
//"Lens: " + getLens();

