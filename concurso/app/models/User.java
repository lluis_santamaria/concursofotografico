package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import play.db.jpa.Model;

/**
 * CLASE QUE DEFINE UN OBJETO USUARIO, @Entity se encarga de hacer un mapeo automatico y relacionar tablas
 * en la base de datos con las librerias import javax.persistence.Entity;
* import javax.persistence.OneToOne;
 * 
 * @author Ismael
 *
 */
@Entity
public class User extends Model{

	 int id_user;
	 String firstname;
	 String lastname;
	 String pass;
	 String email;
	 String dni;
	 String role;
	 boolean voto; //SI ES FALSE SIGNIFICA QUE NO HA VOTADO
	 boolean subida_foto; //SI ES FALSE SIGNIFICA QUE NO HA SUBIDO NINGUNA FOTO
	 public int nfaces=1; //numero de cara subidas
	 public boolean entrenar=false;
	boolean mail_validado=false;
	
	@OneToOne
	public Photo p;
	/**
	 * CONTRUCTOR PARA INSTANCIAR UN OBJETO USUARIO VACIO
	 */
	public User()
	{
		
	}
	/**
	 * CONSTRUCTOR PARA CREAR UN OBJETO USUARIO LLENANDOLO CON DATOS
	 * 
	 * @param i id del usuario
	 * @param f Primer nombre
	 * @param l Apellido
	 * @param p Contraseña
	 * @param e email
	 * @param b
	 * @param r
	 */
	public User(int i, String f, String l, String p, String e, Date b, Date r)
	{
		this.id_user = i;
		this.firstname = f;
		this.lastname = l;
		this.pass = p;
		this.email = e;
		
		
	}
	
	
	public boolean isMail_validado() {
		return mail_validado;
	}

	public void setMail_validado(boolean mail_validado) {
		this.mail_validado = mail_validado;
	}

	public String getRole() {
		return role;
	}
	public void incremento()
	{
		this.nfaces++;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public String getDni() 
	{
		return dni;
	}

	public void setDni(String dni)
	{
		this.dni = dni;
	}
	public int getId_user() {
		return id_user;
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public Photo getP() {
		return p;
	}

	public void setP(Photo p) {
		this.p = p;
	}

	public boolean getVoto() {
		return voto;
	}

	public void setVoto(boolean voto) {
		this.voto = voto;
	}

	public boolean getSubida_foto() {
		return subida_foto;
	}

	public void setSubida_foto(boolean subida_foto) {
		this.subida_foto = subida_foto;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	
	

}
