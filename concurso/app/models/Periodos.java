package models;
import java.awt.Image;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import play.db.jpa.Model;


@Entity
public class Periodos extends Model {
	
	public Periodos() {
		super();
	}
	int id_periodo;
	Calendar fecha_inicial_subida_foto;
	Calendar fecha_final_subida_foto;
	Calendar fecha_inicial_voto;
	Calendar fecha_final_voto_foto;
	
	
	
	public int getId_periodo() {
		return id_periodo;
	}
	public void setId_periodo(int id_periodo) {
		this.id_periodo = id_periodo;
	}
	public Calendar getFecha_inicial_subida_foto() {
		return fecha_inicial_subida_foto;
	}
	public void setFecha_inicial_subida_foto(Calendar fecha_inicial_subida_foto) {
		this.fecha_inicial_subida_foto = fecha_inicial_subida_foto;
	}
	public Calendar getFecha_final_subida_foto() {
		return fecha_final_subida_foto;
	}
	public void setFecha_final_subida_foto(Calendar fecha_final_subida_foto) {
		this.fecha_final_subida_foto = fecha_final_subida_foto;
	}
	public Calendar getFecha_inicial_voto() {
		return fecha_inicial_voto;
	}
	public void setFecha_inicial_voto(Calendar fecha_inicial_voto) {
		this.fecha_inicial_voto = fecha_inicial_voto;
	}
	public Calendar getFecha_final_voto_foto() {
		return fecha_final_voto_foto;
	}
	public void setFecha_final_voto_foto(Calendar fecha_final_voto_foto) {
		this.fecha_final_voto_foto = fecha_final_voto_foto;
	}
	
	
}
