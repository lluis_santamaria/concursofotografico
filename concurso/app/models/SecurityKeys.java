package models;

import java.nio.file.Path;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;


import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.signers.PSSSigner;
import org.bouncycastle.jce.X509Principal;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.AttributeCertificateHolder;
import org.bouncycastle.x509.AttributeCertificateIssuer;
import org.bouncycastle.x509.X509Attribute;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V2AttributeCertificate;
import org.bouncycastle.x509.X509V2AttributeCertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;



import play.db.jpa.Model;
import sun.security.x509.AlgorithmId;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateIssuerName;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateSubjectName;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;



public class SecurityKeys extends Model 
{

	static RSAPrivateKey priv;
	static PublicKey pub;

	
	static CipherParameters privadaCA;
	static CipherParameters publicaCA;
	
	static RSAPrivateKey privCA;
	static RSAPublicKey pubCA;
	
	static Signature sig =null;

	static AsymmetricCipherKeyPair clavesUser ;
	static AsymmetricCipherKeyPair clavesCA ;

	@SuppressWarnings("deprecation")
	static X509V1CertificateGenerator v1CertGen = new X509V1CertificateGenerator();
	static X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();

/*
	static void generar_clavesCA() throws NoSuchAlgorithmException, NoSuchProviderException
	{

		//Generamos un par de claves de 1024 bits con el algoritmo RSA

		KeyPairGenerator kpg=null;

		
		Security.addProvider(new BouncyCastleProvider());

		kpg = KeyPairGenerator.getInstance("RSA","BC");
		kpg.initialize(1024);

		KeyPair kp = kpg.generateKeyPair();
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

		
		kpg.initialize(1024, random);

		random = SecureRandom.getInstance("SHA1PRNG", "SUN");

		kpg.initialize(1024, random);

		priv = (RSAPrivateKey)kp.getPrivate();
		pub = (RSAPublicKey)kp.getPublic();





	}
*/
	
	private static void obtener_publica_CA() throws NoSuchAlgorithmException
	{
		//----OBTENEMOS CERTIFICADO CA-------//
		Certificate cert_CA = null;
		InputStream inStream = null;
	
		try 
		{
			//Cargamos el certificado de la CA para extraerle la clave pública
			inStream = new FileInputStream("C:/cer/certificadoCA.cer");
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			
			//Obtenemos un objeto del tipo Certificate con el certificado de la CA
			cert_CA = cf.generateCertificate(inStream);

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		

		} catch (CertificateException e) {

			e.printStackTrace();
			
		} 

		finally 
		{
			if (inStream != null) 
			{
				try 
				{
					inStream.close();
				} 
				catch (IOException e) 
				{

					e.printStackTrace();

				}
			}
		}
		
		//Convertimos el certificado obtenido en una instancia, X509, para poder manejar 
		//sus datos

		X509Certificate certificado_CA = (X509Certificate)cert_CA;


		 publicaCA = (CipherParameters)cert_CA.getPublicKey();
		 
		 //publicaCA = (CipherParameters)pub;

		
		 
		 
		 
	}
	
	public static void main (String[] args) throws Exception
	{
		try 
		{
			//Generamos las claves RSA para el usuario
			generar_claves(1024);
			//generar_clavesCA();

			//votar();
			//generarCertificadoUsuario(new Date("20/06/2014"),3,"PSEUDO");
			//X509Certificate a = crearCertificadoUser(pub,privCA,pubCA);

			System.out.println("CERTIFICADO CRADO : \n");
		} 
		catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		/*try 
		{
			crear_voto(2);
			//generateCertificate(new Date("20/06/2014"),3,"PSEUDO");

		} catch (ParserConfigurationException | SAXException | IOException
				| TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		obtener_publica_CA();



		build_cert("3","20","PAPA","23/05/2014","23/08/2014","YO",clavesUser.getPublic().toString(),"4435235425422344545","2232");


		byte[] cert_bytes = leer_cert("C:/cer/fitxeer.txt");

		byte[] cert_cegado = cegar(cert_bytes);
		
		

	}


	static void generar_claves(int longitud_clave) throws NoSuchAlgorithmException, NoSuchProviderException
	{

		//Generamos un par de claves de 1024 bits con el algoritmo RSA

		RSAKeyPairGenerator r = new RSAKeyPairGenerator();

		r.init(new RSAKeyGenerationParameters(new BigInteger("10001", 16), new SecureRandom(),
				longitud_clave, 80));

		clavesUser = r.generateKeyPair();

	}

	
	private static byte[] cegar(byte[] cert_a_cegar)
	{

		
		byte[] certFirmadoDescegado=null;
		byte[] CertFirmadoPorCA= null;
		byte[] CertFirmadoDescegado= null;
		
		try
		{


			Cegado c= new Cegado();

			//----------- Usuario: Genera el factor de cegado basado en la clave pública del validador -----------//
			/**BigInteger FactorCegado = c.generateBlindingFactor(clavesCA.getPublic());**/
			BigInteger FactorCegado = c.generateBlindingFactor(publicaCA);
			//----------------- Usuario: ciega el mensaje con el factor de cegado -----------------//
			/**byte[] CertCegado =c.cegar(clavesCA.getPublic(), FactorCegado, cert_a_cegar);*/
			byte[] CertCegado =c.cegar(publicaCA, FactorCegado, cert_a_cegar);

			/*----------OPCIONAL--------------*/

			/*
			//------------- Usuario: Firma el mensaje con su clave privada -------------
			byte[] sig = c.sign(clavesUser.getPrivate(), blinded_msg);

			------------- CA: Verifica la firma del usuario -------------
			if (c.verify(clavesUser.getPublic(), blinded_msg, sig)) 
			{*/

			/*---------------------------------*/


			//---------- CA: Firma el certificado con su clave privada ----------//
			/**CertFirmadoPorCA =c.signBlinded(clavesCA.getPrivate(), CertCegado);*/
			CertFirmadoPorCA =c.signBlinded(privadaCA, CertCegado);

			//------------------- Usuario: Desciega la firma -------------------//
			/**CertFirmadoDescegado =c.descegar(clavesCA.getPublic(), FactorCegado, CertFirmadoPorCA);*/
			CertFirmadoDescegado =c.descegar(publicaCA, FactorCegado, CertFirmadoPorCA);
			
			
			//---------------- Usuario: Verifica la firma descegada de  CA ----------------//
			/**System.out.println(c.verify(clavesCA.getPublic(), cert_a_cegar,certFirmadoDescegado));*/
			System.out.println(c.verify(clavesCA.getPublic(), cert_a_cegar,certFirmadoDescegado));
			String msg2 = new String(certFirmadoDescegado);

			//Ahora el usuario tiene la firma del contenido inicial
			//}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return CertFirmadoDescegado;

	}
	
	
	/**
	 * Para leer la propuesta de certificado del usuario
	 * y pasarlo a bytes para poder cegarlo
	 * 
	 * @param ruta
	 * 
	 * 	 */
	private static byte[] leer_cert(String ruta)
	{

		Path p;
		FileInputStream fileInputStream=null;

		File file = new File(ruta);

		byte[] bFile = new byte[(int) file.length()];

		try 
		{

			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

			for (int i = 0; i < bFile.length; i++) {
				System.out.print((char)bFile[i]);
			}

			System.out.println("Done");
		}
		catch(Exception e)
		{
			e.printStackTrace();


		}
		return bFile;


	}

	/**
	 * Construimos el fichero con el contenido del certificado
	 * del usuario
	 * 
	 * @param version1
	 * @param SerialNumber1
	 * @param IssuerDN1
	 * @param fechainicial1
	 * @param fechaexpiracion1
	 * @param SubjectDN1
	 * @param PK1
	 * @param modulus1
	 * @param exponent1
	 */
	private static void build_cert(String version1, String SerialNumber1, String IssuerDN1, String fechainicial1, String fechaexpiracion1,
			String SubjectDN1, String PK1, String modulus1, String exponent1)

	{
		String Version = "Version: "+version1;
		String SerialNumber = "SerialNumber: "+ SerialNumber1; 
		String IssuerDN = "IssuerDN: "+IssuerDN1;
		String fechainicial = "Start Date: "+fechainicial1;
		String fechaexpiracion = "Final Date: "+fechaexpiracion1;
		String SubjectDN = "SubjectDN: "+SubjectDN1;
		String PK = "Public Key: "+PK1;
		String modulus = "modulus: "+modulus1;
		String exponent = "public exponent: "+exponent1;


		try {

			String[] contenido = new String[9];

			contenido[0] = Version;
			contenido[1] = SerialNumber;
			contenido[2] = IssuerDN;
			contenido[3] = fechainicial;
			contenido[4] = fechaexpiracion;
			contenido[5] = SubjectDN;
			contenido[6] = PK;
			contenido[7] =modulus;
			contenido[8] = exponent;


			File file = new File("C:/cer/fitxeer.txt");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			int i =0;

			while(i<9)

			{
				if(i==0)
				{
					bw.write(contenido[i]);
				}
				else
				{
					bw.write(System.getProperty("line.separator"));
					bw.write(contenido[i]);
				}

				i++;
			}
			bw.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}


	}
	static SecretKey key;



	@SuppressWarnings("deprecation")
	public static X509Certificate crearCertificadoUser(PublicKey paramPublicKey1, PrivateKey paramPrivateKey, PublicKey paramPublicKey2)
			throws Exception
			{
		String str = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
		Hashtable localHashtable = new Hashtable();
		Vector localVector = new Vector();
		localHashtable.put(X509Principal.C, "AU");
		localHashtable.put(X509Principal.O, "The Legion of the Bouncy Castle");
		localHashtable.put(X509Principal.L, "Melbourne");
		localHashtable.put(X509Principal.CN, "Eric H. Echidna");
		localHashtable.put(X509Principal.EmailAddress, "feedback-crypto@bouncycastle.org");
		localVector.addElement(X509Principal.C);
		localVector.addElement(X509Principal.O);
		localVector.addElement(X509Principal.L);
		localVector.addElement(X509Principal.CN);
		localVector.addElement(X509Principal.EmailAddress);
		v3CertGen.reset();
		v3CertGen.setSerialNumber(BigInteger.valueOf(20L));
		v3CertGen.setIssuerDN(new X509Principal(str));
		v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 2592000000L));
		v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + 2592000000L));
		v3CertGen.setSubjectDN(new X509Principal(localVector, localHashtable));
		v3CertGen.setPublicKey(paramPublicKey1);
		v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
		v3CertGen.addExtension(MiscObjectIdentifiers.netscapeCertType, false, new NetscapeCertType(48));

		X509Certificate localX509Certificate = v3CertGen.generateX509Certificate(paramPrivateKey);



		byte [] sig = localX509Certificate.getSignature();



		localX509Certificate.checkValidity(new Date());
		localX509Certificate.verify(paramPublicKey2);
		return localX509Certificate;
			}





//	@SuppressWarnings("deprecation")
//	private static void generarCertificadoUsuario(Date expiry,int SerialNumber,String dName) throws CertificateEncodingException
//	{
//
//		Date startDate = new Date();              
//		Date expiryDate = expiry;             
//		BigInteger serialNumber = BigInteger.valueOf(3);   
//		X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
//
//		X500Principal   dnName = new X500Principal("CN="+dName);
//		X500Principal NameCA = new X500Principal("CN = CA CONCURSO FOTOS");
//
//		certGen.setSerialNumber(serialNumber);
//		certGen.setIssuerDN(NameCA);
//		certGen.setNotBefore(startDate);
//		certGen.setNotAfter(expiryDate);
//		certGen.setSubjectDN(dnName);                       
//		certGen.setPublicKey(clavesUser.getPublic());
//
//		certGen.setSignatureAlgorithm("SHA1withRSA");
//		String a = certGen.toString();
//
//		System.out.println("----------CERT STRING = "+a);
//
//		X509V1CertificateGenerator certGen1 = new X509V1CertificateGenerator();
//
//
//
//		X509Certificate cert_x509=null;
//		try {
//			cert_x509 = certGen.generate(privCA, "BC");
//		} catch (CertificateEncodingException | InvalidKeyException
//				| IllegalStateException | NoSuchProviderException
//				| NoSuchAlgorithmException | SignatureException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//		/*byte[] certificado_codificado = cert_x509.getEncoded();
//		System.out.println("\n\n*****************\n LONGITUD CERTIFICAT ABANS D'ENVIAR: "+certificado_codificado.length);
//
//
//		FileOutputStream fos;
//		try {
//			fos = new FileOutputStream("C:/cer/certificatPSEUDO.cer");
//			fos.write(certificado_codificado);
//			fos.flush();
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 
//
//		System.out.println("Finalizando .........\n");
//		boolean verificado= true;
//
//		try
//		{
//			cert_x509.verify(pubCA);
//		}
//		catch(Exception e)
//		{
//			System.out.println("ERROR VERIFICACION");
//			verificado = false;
//		}
//		System.out.println("Resultado verificacion:"+verificado);
//
//	}   
//
//
//	public SecurityKeys() {
//		super();
//	}
//
//	public int numero;
//
//
//
//
//	static void generar_clavesCA() throws NoSuchAlgorithmException, NoSuchProviderException
//	{
//
//		//Generamos un par de claves de 1024 bits con el algoritmo RSA
//
//		KeyPairGenerator kpg=null;
//
//		Security.addProvider(new BouncyCastleProvider());
//
//	    kpg = KeyPairGenerator.getInstance("RSA","BC");
//	    kpg.initialize(1024);
//
//	    KeyPair kp = kpg.generateKeyPair();
//	    SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//
//	    kpg.initialize(1024, random);
//
//		random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//
//		kpg.initialize(1024, random);
//
//		privCA = (RSAPrivateKey)kp.getPrivate();
//		pubCA = (RSAPublicKey)kp.getPublic();
//
//
//
//
//
//	}








	static void firmar_certificado()
	{ 


	}


	static void crear_voto_firmado( byte[] firma) throws TransformerException, 
	ParserConfigurationException, SAXException, IOException
	{



		//File fXmlFile = new File("C:/files_voto/file.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbFactory.newDocumentBuilder();

		//Document doc = dBuilder.parse(fXmlFile);
		//doc.getDocumentElement().normalize();

		Document doc = db.newDocument();


		//Creamos el fichero xml con el campo voto donde irá su valor


		Element firmaElement = doc.createElement("Firma");
		doc.appendChild(firmaElement);
		String firma_coded = Base64.encodeBase64String(firma.toString().getBytes());

		firmaElement.appendChild(doc.createTextNode(firma_coded));

		Attr attr2 = doc.createAttribute("id");
		attr2.setValue("Firma");
		firmaElement.setAttributeNode(attr2);

		DOMSource source = new DOMSource(doc);

		StreamResult result = new StreamResult(new File("C:/files_voto/file_signed.xml"));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.transform(source, result);



	}

	static void votar() throws CertificateEncodingException
	{
		String nombre_usuario="Louis";


		//User u = User.find("byId_user").first();
		int NumberCegado;
	}




	static void crear_voto(int foto_votada) throws ParserConfigurationException, SAXException, 
	IOException, TransformerException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException
	{

		//Instanciamos las clases para crear el voto XML



		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 


		//Creamos el fichero xml con el campo voto donde irá su valor

		Document doc = db.newDocument();


		//Asignamos la votación elegida
		Element rootElement = doc.createElement("Voto");
		doc.appendChild(rootElement);

		Attr attr = doc.createAttribute("id");
		attr.setValue("idvotacion");
		rootElement.setAttributeNode(attr);

		rootElement.appendChild(doc.createTextNode(Integer.toString(foto_votada)));

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("C:/files_voto/file.xml"));



		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.transform(source, result);

		firmar_voto(doc,"file.xml");




	}

	static void firmar_voto(Document d,String nombre) throws NoSuchAlgorithmException, NoSuchProviderException, 
	InvalidKeyException, SignatureException, IOException, TransformerException, 
	ParserConfigurationException, SAXException
	{
		Security.addProvider(new BouncyCastleProvider());
		sig = Signature.getInstance("SHA1withRSA", "BC");
		sig.initSign(priv);

		FileInputStream fis = new FileInputStream("C:/files_voto/"+nombre);
		BufferedInputStream bufin = new BufferedInputStream(fis);
		byte[] buffer = new byte[1024];
		int len;
		while ((len = bufin.read(buffer)) >= 0) {
			sig.update(buffer, 0, len);
		};
		bufin.close();

		byte[] firma = sig.sign();

		crear_voto_firmado(firma);


	}
}
/* ************************************************* MÈTODES NO USATS ****************************************-/

private static void generar_clavesCA()
		{

			KeyPairGenerator kpg=null;

			try 
			{
				Security.addProvider(new BouncyCastleProvider());

				kpg = KeyPairGenerator.getInstance("RSA","BC");
				kpg.initialize(1024);

				KeyPair kp = kpg.generateKeyPair();
				SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

				kpg.initialize(1024, random);

				random = SecureRandom.getInstance("SHA1PRNG", "SUN");

				kpg.initialize(1024, random);

				privCA = (RSAPrivateKey)kp.getPrivate();
				pubCA = (RSAPublicKey)kp.getPublic();

			} 
			catch (NoSuchAlgorithmException | NoSuchProviderException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}







---------------------
static void generar_claves() throws NoSuchAlgorithmException, NoSuchProviderException
	{

		//Generamos un par de claves de 1024 bits con el algoritmo RSA

		KeyPairGenerator kpg=null;

		Security.addProvider(new BouncyCastleProvider());

		kpg = KeyPairGenerator.getInstance("RSA","BC");
		kpg.initialize(1024);

		KeyPair kp = kpg.generateKeyPair();
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

		kpg.initialize(1024, random);

		random = SecureRandom.getInstance("SHA1PRNG", "SUN");

		kpg.initialize(1024, random);

		priv = (RSAPrivateKey)kp.getPrivate();
		pub = (RSAPublicKey)kp.getPublic();





	}

	---------------------
public static void cegar(byte[] cert) throws InvalidKeyException, SignatureException, 
NoSuchAlgorithmException, NoSuchProviderException
{

	CegadoRSA r = new CegadoRSA();
	AsymmetricCipherKeyPair keys_cegado = r.generateKeys(1024);

	RSABlindingEngine blinding = new RSABlindingEngine();

	blinding.init(true, keys_cegado.getPublic());











}
 public static byte[] blind(CipherParameters key, BigInteger factor, byte[] msg) 
    {
        RSABlindingEngine eng = new RSABlindingEngine();



        RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters)key, factor);
        PSSSigner blindSigner = new PSSSigner(eng, new SHA1Digest(), 15);
        blindSigner.init(true, params);

        blindSigner.update(msg, 0, msg.length);

        byte[] blinded = null;
        try {
            blinded = blindSigner.generateSignature();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return blinded;
    }


 public static int getInt(byte[] array, int offset) {
	    return
	      ((array[offset]   & 0xff) << 24) |
	      ((array[offset+1] & 0xff) << 16) |
	      ((array[offset+2] & 0xff) << 8) |
	       (array[offset+3] & 0xff);
	  }
 public static byte[] int2byte(int[]src) {
	    int srcLength = src.length;
	    byte[]dst = new byte[srcLength << 2];

	    for (int i=0; i<srcLength; i++) {
	        int x = src[i];
	        int j = i << 2;
	        dst[j++] = (byte) ((x >>> 0) & 0xff);           
	        dst[j++] = (byte) ((x >>> 8) & 0xff);
	        dst[j++] = (byte) ((x >>> 16) & 0xff);
	        dst[j++] = (byte) ((x >>> 24) & 0xff);
	    }
	    return dst;
	}
private static void decryptCert(byte[] encrypted,int enc_len)
{
	Cipher cipher;
	byte[] decrypted=null;
	int dec_len=0;
	try 
	{
		cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		//byte[] keyBytes = "Clave de encriptacion".getBytes();
		byte[] ivBytes= new byte[64];


		//SecretKeySpec key = new SecretKeySpec(keyBytes, "DES");
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);

		cipher.init(Cipher.DECRYPT_MODE, key,ivSpec);
		decrypted = new byte[cipher.getOutputSize(enc_len)];
		dec_len = cipher.update(encrypted, 0, enc_len, decrypted, 0);

		dec_len += cipher.doFinal(decrypted, dec_len);


	} 
	catch (NoSuchAlgorithmException | NoSuchPaddingException 
			| ShortBufferException| InvalidKeyException 
			| IllegalBlockSizeException 
			| BadPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvalidAlgorithmParameterException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}


	 private static void encryptCert() throws NoSuchAlgorithmException, NoSuchPaddingException,
	InvalidKeyException, InvalidAlgorithmParameterException, ShortBufferException, IllegalBlockSizeException,
	BadPaddingException
	{


		FileInputStream fileInputStream=null;

        File file = new File("C:/cer/fitxeer.txt");

        byte[] bFile = new byte[(int) file.length()];

        try 
        {
	            //convert file into array of bytes
		    fileInputStream = new FileInputStream(file);
		    fileInputStream.read(bFile);
		    fileInputStream.close();

		    for (int i = 0; i < bFile.length; i++) {
		       	System.out.print((char)bFile[i]);
	            }

		    System.out.println("\nDone");
	    }
        catch(Exception e)
        {
	        e.printStackTrace();
	    }


        try {
			cegar(bFile);
		} catch (SignatureException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}



		//System.out.println("DESENCRIPTAT :"+decrypted.toString());


	}
	private static void sign_Cert()
	{




	}

		/*static void encriptar_datos(String nombre_usuario, pub)
	{
		byte[] input;
		byte[] keyBytes;
		byte[] ivBytes;




	}


 */