package notifiers;

import models.User;

import org.apache.commons.mail.*; 
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import play.*;
import play.mvc.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import play.libs.Mail;
/**
 * CLASE JAVA QUE GENERA Y ENVIA MAILING PARA ABRIR COMUNICACION PERSONAL CON LOS USUARIOS
 * 
 * @author Ismael
 *
 */
public class Mails
{
		/**
		 * Metodo que envia un correo de bienvenida a un nuevo usuario y le entrega una url para validar
		 * su correo electronico
		 * 
		 * @param user
		 * @return retornamos un 0 si no ha saltado ningun catch, enviamos error con int negativos
		 * @throws EmailException Controlamos la malformacion de un correo electronico "algo@algo"
		 * @throws MalformedURLException Contorlamos la malformacion de urls
		 */
		public static int welcome(User user) throws EmailException, MalformedURLException 
		{
			try
			{
				
				//ENVIAR UN CORREO SOLO TEXTO		
				String parametros=user.getEmail();
				Codificador codec = new Codificador();
				String parametros_codificados= codec.codificar_array(parametros);
		    	SimpleEmail email = new SimpleEmail();
		    	email.setFrom("support@photoconcurso.com");
		    	email.addTo(user.getEmail());
		    	email.setSubject("Validación de cuenta");
		    	email.setMsg("Bienvenido "+user.getFirstname()+" " +user.getLastname()+", Porfavor Valida tu correo electronico clickando en http://192.168.1.177:9999/tracking_functions/validador_user?params="+parametros_codificados);
		    	Mail.send(email);
	    	
	    	
			//ENVIAR UN CORREO CON UN HTML
//	    	HtmlEmail email = new HtmlEmail();
//	    	email.addTo("tech@makemailing.com");
//	    	email.setFrom("send@tracklead.es", "Soporte Picking Tool");
//	    	email.setSubject("Bienvenido");
//	    	
//	    	// embed the image and get the content id
//	    	URL url = new URL("https://pbs.twimg.com/profile_images/1864624768/LogoMM.jpg");//logo
//	    	String cid = email.embed(url, "Zenexity logo");
//	    	
//	    	// set the html message
//	    	
//	    	email.setHtmlMsg("<html>Zenexity logo - <img src=\"cid:"+cid+"\"></html>");
//	    	
//	    	// set the alternative message
//	    	//email.setTextMsg("Your email client does not support HTML, too bad :(");
//	    	Mail.send(email);
	    	
	    	return 0; //todo ok
	    	
			}
			catch(EmailException em)
			{
				//Aqui controlamos los fallos del estilo correos que no tienen la @, sin embargo correos inexistentes nos los
				//tragamos, para controlar eso tenemos que hacer el validador
				System.err.println("Fallo en el formato del email CLASE MAILS PACKAGE NOTIFIERS function Welcome");
				return -1;//fallo
			}
		
		
		}
		/**
		 * Metodo que envia un correo con el password del usuario
		 * 
		 * @param e_mail email donde recibira el password
		 * @return retornamos un 0 si no ha saltado ningun catch, enviamos error con int negativos
		 */
		public static int lostPassword(String e_mail) 
		{
			User user=User.find("byEmail", e_mail).first();
			
			if(user!=null)
			{
				try
				{
					SimpleEmail email = new SimpleEmail();
			    	email.setFrom("support@photoconcurso.com");
			    	email.addTo(user.getEmail());
			    	email.setSubject("Recupera tu contraseña");
			    	email.setMsg("Bienvenido "+user.getFirstname()+"," +
			    			" Tu contraseña es: "+user.getPass());
			    	Mail.send(email);
			    	
					return 0;
				}
				catch (EmailException em)
				{
					System.err.println("Fallo en el formato del email CLASE MAILS PACKAGE NOTIFIERS function lostPassword");
					return -2;
				}
			}
			else
			{
				//FALLO; NO EXISTE ESTE CORREO EN NUESTRA BASE DE DATOS
				System.err.println("No existe el correo " +e_mail+" en la base de datos CLASE MAILS PACKAGE NOTIFIERS function lostPassword");
				return -1;
			}
		}
		   
	
		
}
