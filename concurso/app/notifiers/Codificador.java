package notifiers;

import java.util.Calendar;

import org.apache.commons.codec.binary.Base64;

/**
 * CLASE JAVA QUE SE ENCARGA DE CODIFICAR Y DECODIFICAR en base64 Strings
 * 
 * @author Ismael
 *
 */
public class Codificador 
{
	/**
	 * Codificamos un String en base64
	 * 
	 * @param Parametros que queremos codificar
	 * @return Parametros codificados en base64
	 */
	public static String codificar_array(String parametros)
	{
		//FUNCIÓN PARA CODIFICAR LOS PARAMETROS EN BASE64
		Base64 base64 = new Base64();
		byte[] parametros_bytes = parametros.getBytes();
		String encodedParams = base64.encodeToString(parametros_bytes);
		System.out.println("los parametros codificados són: " +encodedParams);
		
		return encodedParams;
	}
	/**
	 * Decodificamos un String en base64
	 * 
	 * @param encodedParams Parametros codificados en base64
	 * @return Parametros decodificados en base64
	 */
	public static String decodificar_array(String encodedParams)
	{
		Base64 base64 = new Base64();
		byte[] decodedBytes = base64.decode(encodedParams);
		String decoded_params = new String(decodedBytes);
		System.out.println("los parametros decodificados són: " +decoded_params);
		
		return decoded_params;
	}
	
}
