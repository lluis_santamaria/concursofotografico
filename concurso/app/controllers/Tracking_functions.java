package controllers;
import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import notifiers.Codificador;
import notifiers.Mails;
import java.io.*;
import java.net.*;

/**
 * CONTROLADOR BASADO EN LA RECOGIDA DE PETICIONES DE TRACKING
 * 
 * @author Ismael
 *
 */
public class Tracking_functions extends Controller 
{
	/**
	 * Renderizamos plantilla lostpass.html
	 * @param flash_error Mensaje de error que le enviamos a la plantilla
	 */
	public static void lostpass(String flash_error)
	{
		render(flash_error);
	}
	/**
	 * Metodo que recoge una peticion http de un usuario, el click viene producido desde su correo,
	 * validamos que el correo con el que se ha registrado existe para darle permisos en la plataforma
	 * 
	 * @param Es el email codificado en base64 del usuario que ha hecho click, necesitamos saber quien ha producido el click
	 */
	public static void validador_user(String params)
	{	
		Codificador codec = new Codificador();
		String emailuser=codec.decodificar_array(params);
		
		User user_check = User.find("byEmail", emailuser).first();
		
		if(user_check!=null)
		{
			user_check.setMail_validado(true);
			user_check.save();
			renderText("Correo validado, eres usuario de nuestra plataforma");
		}
		else
		{
			renderText("Tu validación a caducado, vuelve a registrarte");
		}
		
	}
	/**
	 * Metodo que recoge la orden de una contraseña perdida, enviamos al mail la contraseña del Usuario
	 * @param email
	 */
	public static void perdida_pass(String email)
	{
		if(email.equals(""))
		{
			lostpass("No dejes el campo en blanco");
		}
		else
		{
			Mails m =new Mails();
			int check = m.lostPassword(email);
			
			if(check==0)
			{
				renderText("le hemos enviado un correo electronico con el password");
			}
			else
			{
				lostpass("El correo no lo tenemos en nuestra base de datos, porfavor chequealo");
			}
		}
	}
}
