package controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import play.cache.Cache;
import javax.imageio.ImageIO;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import models.Periodos;
import models.User;
import models.Photo;
import play.mvc.Controller;
import sun.misc.BASE64Decoder;

/**
 * CONTROLADOR QUE ESCUCHA PETICIONES HTTP DE LA APLICACION ANDROID
 * 
 * @author Ismael
 *
 */
public class AndroidServer extends Controller{

	
	/**
	 * Metodo que abre un socket cuando un usuario introduce su password y su username y conincide 
	 * con los datos de la base de datos MYSQL
	 * 
	 * @param username El nombre de usuario introducidos en la app Android
	 * @param password La contraseña del usuario introducidos en la app Android
	 */
	public static void login(String username, String password)
	{
		System.out.println(username);
		System.out.println(password);
		
		Periodos fecha_periodo = Periodos.find("byId_periodo", 0).first();
		System.out.println("Periodos: ");
		System.out.println("Dia que empieza el periodo de la subida de fotos: "+fecha_periodo.getFecha_inicial_subida_foto().getTime());
		System.out.println("Dia que acaba el periodo de la subida de fotos: "+fecha_periodo.getFecha_final_subida_foto().getTime());
		System.out.println("Dia que empiezan los periodos los votos: "+fecha_periodo.getFecha_inicial_voto().getTime());
		System.out.println("Dia que acaba el periodo de los votos: "+fecha_periodo.getFecha_final_voto_foto().getTime());
		
		Calendar fecha_actual=null;
		fecha_actual=fecha_actual.getInstance();
		//Buscsamos en la base de datos si el usuario existe
		User user = User.find("byEmailAndPass",username,password).first();
		
		//ESTA REGISTRADO
		if(user!=null)
		{
			if(fecha_actual.before(fecha_periodo.getFecha_final_subida_foto())==true)
			{
				//ESTAMOS EN EL PERIODO DE SUBIDA DE FOTOS
				
		//////////////FORMAT statelogin:periodo:¿ha subido foto?:¿ha votado?/////////////////
				
				if(user.getSubida_foto()==false)
				{
					//NO HA SUBIDO FOTO
					renderText("ok:PF:UP_FALSE:UP_FALSE");
				}
				else
				{
					//YA HA SUBIDO FOTO
					
					
					renderText("ok:PF:UP_TRUE:UP_FALSE");
				}
				
			}
			else
			{
				//PERIODO FUERA DE SUBIDA DE FOTOS
				
				if(fecha_actual.before(fecha_periodo.getFecha_final_voto_foto())==true)
				{
					//ESTAMOS EN EL PERIODO DE VOTACION
					
					if(user.getVoto()==false)
					{
						//NO HA VOTADO
						if(user.getSubida_foto()==false)
						{
							//NO HA VOTADO y no ha subido foto
							renderText("ok:PV:UP_FALSE:UP_FALSE");
						}
						else
						{
							//NO HA VOTADO y ha subido foto
							renderText("ok:PV:UP_TRUE:UP_FALSE");
							
						}
					}
					else
					{
						//YA HA VOTADO
						renderText("ok:PF:UP_TRUE:UP_TRUE");
					}
				}
				else
				{
					//ESTAMOS FUERA DEL PERIODO DE VOTACIÓN
					renderText("ok:ko");
					
				}
				
			}
				
		}
		else
		{
			//NO ESTA REGISTRADO
			
			renderText("error");
		}		
	}
	/**
	 * Metodo que envia a la App un String con todas las coordenadas de todas las fotos
	 * que estan en la base de datos MSQL separados por un caracter ':'
	 * 
	 */
	public static void mapa()
	{
		//FORMATO (LATITUD/LONGITUD:LATITUD/LONGITUD)
		int i=0;
		String coordenadas="";
		List<Photo> Lphotos = Photo.findAll();
		
		while(i<Lphotos.size())
		{
			coordenadas=Lphotos.get(i).getCoordenadas()+":"+coordenadas;
			i++;
		}
		System.out.println(coordenadas);
		renderText(coordenadas);
	
	}
	
	/**
	 * Metodo que recoge una foto codificada en base64, la decodifica, la guarda en una carpeta, y guarda el 
	 * Path en la base de datos con información adicional
	 * 
	 * @param email Email del usuario, es la key de id de los users
	 * @param foto foto codificada en base64
	 * @param localizacion localización de donde se tomo la foto longitud|altitud
	 */
	public static void guardar_foto(String email, String foto, String localizacion)
	{
		//NO HE PODIDO COMPROBAR SI FUNCIONA O NO by:ISMA
		//FUNCIO QUE SENCARREGA DE GUARDAR LA FOTO QUE EL PARTICIPANT HA FET DESDE EL MOBIL
		
		System.out.println(email);
		System.out.println(foto);
		System.out.println(localizacion);
		Photo foto_android = new Photo();
		User user = User.find("ByEmail", email).first();
		List<Photo> Lphoto = Photo.findAll();
		int Nphoto = Lphoto.size();
		String ruta_guardado = "D:/UPC/4A/cities2/ProyectoConcurso/trunk/ProjecteServidor/concurso/public/imagesUsers/" + Nphoto + ".jpg";
		File outputfile = new File(ruta_guardado);
		byte[] imageByte;
		   
		    try {
		    	
				BufferedImage image = null;
		        BASE64Decoder decoder = new BASE64Decoder();
		        imageByte = decoder.decodeBuffer(foto);
		        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		        image = ImageIO.read(bis);
		        ImageIO.write(image, "jpg", outputfile);
		        bis.close();
		        
		        foto_android.setPath("/public/imagesUsers/" + Nphoto + ".jpg");
		        foto_android.setCoordenadas(localizacion);
		        foto_android.setid_photo(user.getId_user());
		        foto_android.save();
		        user.setSubida_foto(true);
		        user.save();
		        
		        renderText("ok");	
		    
		    } catch (Exception e) 
		    {
		        e.printStackTrace();
		        renderText("ERROR");	
		    }
		    
		  
		}
	/**
	 * Metodo que envia a la app una lista con todas las fotos de la base de datos
	 * el String que le enviamos tiene formato
	 * 
	 * Numero_de_fotos@foto.codificada.base64@next_photo@....
	 * 	
	 */
	public static void Get_Photos()
	{
		//FUNCIO QUE ENVIA UNA LLISTA DE FOTOS AL ANDROID PQ AQUEST PUGUI VOTA DESDE EL MOBIL SI VOL
		System.out.println("Entramos Get_photos del controlador Android");
		String Photos="";
		String PhotoREAD="";
		int i =0;
		Photo p=new Photo();
		List<Photo> fotos = p.percent_show_algorithm();//enviamos el 25% de las fotos menos mostradas 
		System.out.println(fotos.size());

		try
		{			
			while(i<fotos.size())
			{
			
				File imgPath = new File("D:/UPC/4A/cities2/ProyectoConcurso/trunk/ProjecteServidor/concurso"+fotos.get(i).PathSIEXIF);
				
				BufferedImage bufferedImage = ImageIO.read(imgPath);
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
				ImageIO.write(bufferedImage, "jpg", baos);
				baos.flush();
				PhotoREAD=Base64.encode(baos.toByteArray());
				baos.close();

				if(i==0)
				{
					Photos = PhotoREAD;
				}
				else
				{
					Photos = Photos+"@"+PhotoREAD; //Separamos las fotos por el caracter @

				}

				i++;
			}
			System.out.println("El String Fotos es: "+Photos);

			renderText(fotos.size()+"@"+Photos); 
			
		}
		catch(Exception e)
		{
			System.out.println("dentro del catch");
			renderText("Fallo en el servidor");
		}


	}

}
