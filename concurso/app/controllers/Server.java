package controllers;

import play.*;
import play.mvc.*;
import java.awt.Image;
import java.util.*;
import notifiers.Mails;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.mail.EmailException;
import org.w3c.dom.*;
import models.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

/**
 * CONTROLADOR QUE ESCUCHA PETICIONES HTTP DE LOS HTML
 * Este controlador funciona a partir de que un usuario abre socket
 * 
 * @author Ismael
 * @author Martí
 *
 */
public class Server extends Controller {

//	@Before
//    static void addUser()
//	{
//        User user = connected();
//        if(user != null) {
//            renderArgs.put("user", user);
//        }
//    }
	
	/**
	 *  Chequea si el usuario tiene conexión abierta en el socket, y obligamos a pasar todas las peticione
	 *  por aquí
	 */
	@Before
    static void checkUser() 
	{
        if(Application.connected() == null) 
        {
            flash.error("Please log in first");
           Application.index();
        }
    }
	/**
	 * Cierra la sesión del socket
	 */
	public static void logout() {

		String Email_session = session.get("email");
		System.out.println("LOGAOUT FUNCTION: Usuario con email " + Email_session+ " ha cerrado la sesión");
		session.remove("email", Email_session);
		Application.index();
	}
	/**
	 * Enviamos la coordenada altitud de una foto concreta del usuario que esta en el socket
	 * @return Retornamos la coordenada altitud en una variable double, el html se comunica con ajax con 
	 * este método
	 */
	public static double altitud(){
		User user=Application.connected();
	   Photo pics = Photo.find("byId_photo", user.getId_user()).first();
	   
	   System.out.println(pics.getCoordenadas());
	   
	   String string = pics.getCoordenadas();
	   String[] parts = string.split(" ");
	   String grados = parts[0]; 
	   double g=Double.parseDouble(grados);
	   
	   String minutos = parts[2];
	   String[] min = minutos.split("'");
	   String minut = min[0];
	   double m=Double.parseDouble(minut);
	   //System.out.println(m/60);
	   
	   String segundos = parts[3];
	   char corte=(char)(34);
	   String cortef=String.valueOf(corte);
	   String[] seg = segundos.split(cortef);
	   String sec = seg[0];
	   System.out.println(sec);
	   double s=Double.parseDouble(sec);
	   //System.out.println(s/3600);
	   
	   System.out.println(g+m/60+s/3600);
	   double total=g+m/60+s/3600;
	   
	return total;
	}
	/**
	 * Función que recoge una petición http del html y se la redirijimos a un applet que esta en 
	 * un servidor apache
	 */
	public static void applet()
	{
		System.out.println("entrem applet");
		redirect("http://localhost:9989/app/");
	}
	/**
	 * 
	 * Calculamos el tiempo restante de los periodos para mostrar al usuario el tiempo que le queda
	 * para votar/subir foto
	 * @return
	 */
	public static int tiempo()
	{
		System.out.println("dins time");
		Periodos time =Periodos.find("byId_periodo", 0).first();
		int ultimo = time.getFecha_final_subida_foto().get(Calendar.MINUTE);
		Calendar cal1 = Calendar.getInstance();
		int ahora= cal1.get(Calendar.MINUTE);
		if((ahora>time.getFecha_inicial_voto().get(Calendar.MINUTE))&&(ahora<time.getFecha_final_voto_foto().get(Calendar.MINUTE)))
		{
			System.out.println(ultimo-ahora);
			return -(ultimo-ahora);
		}
		return 0;
	}
	/**
	 * Renderizamos un plantilla html "camara.html"
	 */
	public static void camera()
	{
		System.out.println("dins de camera");
		render();
	}
	/**
	 * Metodo de reconocimiento facial, entrenar hace una media de 2 fotos de las cuales
	 * compararemos con la foto target (foto con la que queremos entrar)
	 * para decidir si el usuario esta registrado o no.
	 * @param id
	 * @return
	 */
	public static String entrenar(int id)

	{
		Process theProcess = null;
	      BufferedReader inStream = null;
	      System.out.println("dins entrenar");
	      try
	      {
	          theProcess = Runtime.getRuntime().exec("java -jar C:\\facerecognizer\\entrenamiento.jar "+id);
	      }
	      catch(IOException e)
	      {
	         System.err.println("Error on exec() method");
	         e.printStackTrace();  
	      }
	      try
	      {
	         inStream = new BufferedReader(
	         new InputStreamReader( theProcess.getInputStream() ));  
	         System.out.println(inStream.readLine());
	         Application.connected().entrenar=true;
	         Application.connected().save();
	      }
	      catch(IOException e)
	      {
	         System.err.println("Error on inStream.readLine()");
	         e.printStackTrace();  
	      }
	      return "ok";
	}
	/**
	 *Enviamos la coordenada latitud de una foto concreta del usuario que esta en el socket
	 *
	 * @return Retornamos la coordenada altitud en una variable double, el html se comunica con ajax con 
	 * este método
	 */
	public static double latitud(){
		
		User user=Application.connected();
		   Photo pics = Photo.find("byId_photo", user.getId_user()).first();
		   
		   System.out.println(pics.getCoordenadas());
		   
		   String string = pics.getCoordenadas();
		   String[] parts = string.split(" ");
		   String grados = parts[5]; 
		   double g=Double.parseDouble(grados);
		   
		   String minutos = parts[7];
		   String[] min = minutos.split("'");
		   String minut = min[0];
		   double m=Double.parseDouble(minut);
		   //System.out.println(m/60);
		   
		   String segundos = parts[8];
		   char corte=(char)(34);
		   String cortef=String.valueOf(corte);
		   String[] seg = segundos.split(cortef);
		   String sec = seg[0];
		   System.out.println(sec);
		   double s=Double.parseDouble(sec);
		   //System.out.println(s/3600);
		   
		   System.out.println(g+m/60+s/3600);
		   double total=g+m/60+s/3600;
		   
		return total;
	}
	/**
	 * Decodificamos en base64 una imagen que viene en un array
	 * @param imageDataString foto codificada en base 64
	 * @return foto decodificada en base64
	 */
	public static byte[] decodeImage(String imageDataString) {
        return Base64.decodeBase64(imageDataString);
    }
	/**
	 * Recibe la foto del proceso entrenamiento, es decir, si el usuario no tiene imagen de entrenamiento
	 * se tiene que hacer dos fotos de entrenamiento, para posteriormente poder hacer el reconocimiento
	 * con la foto target
	 * 
	 * @param datos foto entrenamiento del html camara.html
	 * @throws IOException
	 */
	public static void mifoto(String datos) throws IOException {
		int id = Application.connected().getId_user();
		System.out.println(id);
		String[] foto2=datos.split(",");
		String foto3=foto2[1];
        try {           
            // Reading a Image file from file system
            
            // Converting a Base64 String into Image byte array
            byte[] imageByteArray = decodeImage(foto3);
             
            // Write a image byte array into file system
            FileOutputStream imageOutFile = new FileOutputStream("C:\\facerecognizer\\data\\images\\training\\"+id+Application.connected().nfaces+".jpg");
            
            imageOutFile.write(imageByteArray);
            imageOutFile.close();
 
            System.out.println("Image Successfully Manipulated!");
        } catch (FileNotFoundException e) {
            System.err.println("Image not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        
		System.out.println("foto echa");
		
		Application.connected().incremento();
		Application.connected().save();
			System.out.println(Application.connected().nfaces);
		
	}
	/**
	 * Guardamos la foto con la que el usuario quiere competir en una carpeta, recogemos los datos del EXIF si es que los tiene
	 * y guardamos en la base de datos los paths
	 * @param path
	 */
	public static void upfoto(File path) {
		// Funcion para guardar la foto en el Servidor y el path en la base de
		// datos
		System.out.println("entramos upfoto" + path);

		Calendar fecha_actual = null;
		fecha_actual=fecha_actual.getInstance();
		Periodos fecha_periodo= Periodos.find("byId_periodo", 0).first();

		System.out.println("Periodos: ");
		System.out.println("Dia que empieza el periodo de la subida de fotos: "+fecha_periodo.getFecha_inicial_subida_foto().getTime());
		System.out.println("Dia que acaba el periodo de la subida de fotos: "+fecha_periodo.getFecha_final_subida_foto().getTime());
		System.out.println("Dia que empiezan los periodos los votos: "+fecha_periodo.getFecha_inicial_voto().getTime());
		System.out.println("Dia que acaba el periodo de los votos: "+fecha_periodo.getFecha_final_voto_foto().getTime());

		int ID = Application.connected().getId_user();
		User user = User.find("byId_user",ID).first();


		try
		{
			BufferedImage bufferedImage = ImageIO.read(path);
			
			String ext = FilenameUtils.getExtension(path.toString());
			System.out.println("FORMATO DEL FICHERO SUBIDO: "+ext);
			
			
			if((ext.equals("jpg"))||(ext.equals("png"))||(ext.equals("JPG"))||(ext.equals("PNG")))
			{
				System.out.println("1");

			if(fecha_actual.before(fecha_periodo.getFecha_final_subida_foto())==true)
			{

				//ESTAMOS EN EL PERIODO DE SUBIDA DE FOTOS

				System.out.println("2");

				if(user.getSubida_foto()==false)
				{
					//EL USUARIO NO HA SUBIDO UNA FOTO, PUEDE SUBIRLA
					System.out.println("3");
					List<Photo> Lphoto = Photo.findAll();
					int Nphoto = Lphoto.size();
					 String ruta_guardado = "D:/UPC/4A/cities2/ProyectoConcurso/trunk/ProjecteServidor/concurso/public/imagesUsers/" + Nphoto + ".jpg";
					// String ruta_guardado = "C:/Users/marti/Documents/Cities/trunk/ProjecteServidor/concurso/public/imagesUsers/" + Nphoto + ".jpg";
					// String ruta_guardado_nexif = "C:/Users/marti/Documents/Cities/trunk/ProjecteServidor/concurso/public/noExifUser/" + Nphoto +".jpg";		 
				 	
					String ruta_guardado_nexif = "D:/UPC/4A/cities2/ProyectoConcurso/trunk/ProjecteServidor/concurso/public/noExifUser/" + Nphoto + ".jpg";
				 	path.renameTo(new File(ruta_guardado));
					Photo PhotoNueva = new Photo();
					PhotoNueva.setPath("/public/noExifUser/" + Nphoto + ".jpg");
					//PhotoNueva.PathSIEXIF="/public/imagesUsers/"+ Nphoto + ".jpg";
					System.out.println("9");
					
					//////////////EXIF///////////////
					ExifToolExtractor exif= new ExifToolExtractor(null);
					System.out.println("8");

					PhotoNueva.setModelo(exif.main(ruta_guardado).getModel());
					PhotoNueva.setExposeture(exif.main(ruta_guardado).getExposureCompensation());
					PhotoNueva.setDate(exif.main(ruta_guardado).getDateTaken());
					PhotoNueva.setFocal(exif.main(ruta_guardado).getFocalLength());
					PhotoNueva.setFlash(exif.main(ruta_guardado).getFlash());
					PhotoNueva.setLens(exif.main(ruta_guardado).getLens());
					PhotoNueva.setCoordenadas(exif.main(ruta_guardado).getGPS());
					/////////////////////////////////////
					System.out.println("ENTERATEEEEEEEEEEEEEEEEEEEEEEE"+exif.main(ruta_guardado).getGPS());

					//Guardamos la imagen en una carpeta llamada PhotosConcurso en el disco local C:/
					File outputfile = new File(ruta_guardado_nexif);
					
					//Comprabamos si la ruta existe en el servidor..
					ImageIO.write(bufferedImage, "jpg", outputfile);//Guardamos la foto en el servidor fisicamente
					//existe_ruta = outputfile.exists();
					
					

					PhotoNueva.setid_photo(ID);
					PhotoNueva.save(); //guardamos los atriburos de la clase foto en la DB
					user.setSubida_foto(true);
					user.save();
					

					System.out.println("foto guardada correctamente");
					//renderText("Tienes que esperar a que estemos en el periodo de voto y podras votar las 3 mejores fotos");				
					siphoto();
				}
				else
				{
					//EL USUARIO YA HA SUBIDO UNA FOTO ANTERIORMENTE
					//renderText("No puedes subir mas fotos, ya la has subido");
					nophoto();
					System.out.println("4");
				}
			}
			else
			{	
				System.out.println("5");
				renderText("ESTAMOS fuera del periodo de subida de fotos, no puedes subir mas fotos");	
			}

		}
		else
		{
			System.out.println("6");
			renderText("No has subido una foto, el formato ha de ser .jpg o .png");
		}
		}
		catch (Exception e) 
		{
			System.out.println("7");
			System.out.println("exception en files");
			renderText("error de formato");
		}	

	}
	/**
	 * Enviamos las coordenadas a un html, el siphoto.html se comunica con ajax con este metodo
	 * @return
	 */
	public static String send_coordenadas()
	{
			//Funcion para el ajax
			System.out.println("FUNCIÓN PARA PETICIONES AJAX ACTIVADA");

			User user=Application.connected();
			Photo pics = Photo.find("byId_photo", user.getId_user()).first();
			return pics.getCoordenadas();
			
	}
	/**
	 * Renderizamos la plantilla setuptemplate.html, hacemos unas comprobaciones para controlar
	 * bugs
	 */
	public static void SetUpTemplate()
	{
		//entrenar(connected().getId_user());
		if(Application.connected().entrenar==true){
			if(Application.connected().getSubida_foto()==false){
				nophoto();
			}else{
				siphoto();
			}
		}else{
			entrenar(Application.connected().getId_user());
			if(Application.connected().getSubida_foto()==false){
				nophoto();
			}else{
				siphoto();
			}
		}
	}
	/**
	 * Renderizamos la plantilla gallerytemplate.html, ademas preparamos información para mostrar en el html
	 * el contador de los periodos
	 */
	public static void galleryTemplate()
    {
		Periodos time = Periodos.find("byId_periodo", 0).first();
		int inicio=time.getFecha_inicial_voto().get(Calendar.MINUTE);
		int fin=time.getFecha_final_voto_foto().get(Calendar.MINUTE);
		Calendar cal1 = Calendar.getInstance();
		int ahora= cal1.get(Calendar.MINUTE);
		System.out.println(inicio+"inicio fin"+fin);
		if(ahora>inicio && ahora<fin){
			Photo p = new Photo();
			List<Photo> pics = p.percent_show_algorithm();//AQUI CREAMOS LA LISTA CON EL 25% DE LAS FOTOS MENOS MUESTRADAS AL USUARIO QUE QUIERE VOTAR
			User user = Application.connected();
			render(pics,user);//Enviamos lista de fotos + el usuario conectado
		}else{
			siphoto();
		}
    }
	/**
	 * Renderizamos la plantilla nophoto.html
	 */
	public static void nophoto(){
	   
		User user= Application.connected();
		render(user);
   }
	/**
	 * Renderizamos la plantilla siphoto.html
	 */
   public static void siphoto(){
	   
	   User user=Application.connected();
	   Periodos fin =Periodos.find("byId_periodo", 0).first();
	   
	   System.out.println("Usuario: "+user.getEmail());
	   System.out.println("ID: "+user.getId_user());
	   
	   Photo pics = Photo.find("byId_photo", user.getId_user()).first();
	   System.out.println(pics);
	   render(pics, user,fin);
   }
}