package controllers;

import java.applet.AppletContext;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.security.cert.CertificateExpiredException;

//import javax.security.cert.X509Certificate;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERUTF8String;

import play.mvc.Controller;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;



public class Security extends Controller
{
	
	final static String ruta_CA= "http://192.168.1.135:7777/CertificationAuthority/";
	final static String ruta_usuario= "http://192.168.1.135:9999/recibir_certCegado_CA";
	final static String ruta_RA = "http://192.168.1.135:8888/ValidacionCertificado/obtener_certificado";
	/*---SERVIDOR WEB CONCURSO----*/

	public static void recibir_certCegado_concurso(String certCegado) throws Exception
	{
		System.out.println("\n-----------------------------\n " +
				"SERVIDOR CONCURSO     " +
				"\n-----------------------------\n");

		System.out.println("SERVER CONCURSO : Recibimos el certificado Cegado por el usuario :\n"+certCegado+"\n");



		concurso_sendPostCertCegadoCA(certCegado);
		//enviarCertCegadoaCA(certCegado);

		/*byte[] certFirmado = firmaCertCegadoCA(certCegado);

		enviar_certCegadoFirmado_CA(certFirmado);*/

	}
	private static void concurso_sendPostCertCegadoCA(String certCegado) throws Exception 
	{
			
		//****????*****------PARaMETROS A ENVIAR----------------//
				
		String urlParameters = "certCegadoB64="+certCegado;
			
		System.out.println("\n ----CONCURSO: Enviamos cert Cegado a CA....-----\n");
		//--------URL---------------//
		String url = ruta_CA+"recibir_certCegado_CA";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//CABECERA
		con.setRequestMethod("POST");
    	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    	
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'POST' a la  URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		

		//print result
		System.out.println("CONCURSO : RESPUESTA A POST DE CERTIFICADO A 'CA' :\n"+response.toString());

		renderText(response.toString());
		in.close();
	}
	
	static String certificado_recibido;

	public static void register_con_certificado(String cert) throws Base64DecodingException, IOException
	{
	
		
	    		
		System.out.println("*---------------------REGISTER------------------------*\n");
		System.out.println("*------------------CON CERTIFICADO --------------------*\n\n\n");
		
		
		
		//String c= params.get("cert");
		System.out.println("Certificado recibido "+cert+"\n\n\n\n");
		System.out.println("Longitud certificado ...."+cert.length());
		
		//Reemplazamos los carácteres vacíos con un + para que siga el formato correcto
		String cert1 = cert.replaceAll(" ", "+");
		
		
		System.out.println("ENVIAMOS CERTIFICADO A RA PARA VALIDACIÓN....");
	
		try {
			sendPost(cert1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//--------PARA GUARDAR CERT EN SERVIDOR -------------
		
		byte[] b = cert1.getBytes();
		//System.out.println("Longitud del certificado en bytes:"+b.length+"\n");
		
		
		byte[] byteMessage = Base64.decodeBase64(b);
		//System.out.println("received MESSAGE byte Length: "+byteMessage.length+"\n");
   	
		  
	    FileOutputStream fos = new FileOutputStream("C:/cer2/certificateUSUARIODNI.cer");
		
		fos.write(b);
		fos.flush();
		fos.close();
		
		
		
					
	}
	
	
	

	
	private void resultado_validacion(String res)
	{
		if(0== Integer.parseInt(res))
		{
			System.out.println("CERTIFICADO VÁLIDO");
		}
		else
			System.out.println("CERTIFICADO NO VÁLIDO");
		
		
	}
	private static X509Certificate obtener_certificado(String cert)
	{
		byte[] b = cert.getBytes();
		System.out.println("Longitud del certificado en bytes:"+b.length+"\n");
		byte[] byteMessage = Base64.decodeBase64(b);
		System.out.println("received MESSAGE byte Length: "+byteMessage.length+"\n");
		
		Certificate a = null;
		try 
		{
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			
			System.out.println("array input es "+b);
			InputStream in = new ByteArrayInputStream(byteMessage);
			
			a = cf.generateCertificate(in);
			
			
		} 
		
		catch (CertificateException e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		
		X509Certificate certx = (X509Certificate)a;
		
		return certx;
		
		
	}
	static void recibir_certFirmado_concurso(String certFirmadoB64)
	{
		//enviar_certFirmado_aUsuario(certFirmadoB64);
		
		renderText(certFirmadoB64);
	}
	
	
	private static void sendPostCertCegadoFirmadoaUsuario(int resultado) throws Exception 
	{
			
		
		
		System.out.println("enviamos el resultado del a validación...1/2");
		
		//****????*****------PARaMETROS A ENVIAR----------------//
		
		
		
		String urlParameters = "resultado="+Integer.toString(resultado);
			
		//--------URL---------------//
		String url = ruta_CA;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//CABECERA
		con.setRequestMethod("POST");
    	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    	
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'POST' a la  URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println("RESPUESTA---------------------------"+response.toString());

		renderText(response.toString());
		
	}
	
	
	private static void sendPost(String certCegado) throws Exception 
	{
			
		//****????*****------PARaMETROS A ENVIAR----------------//
				
		String urlParameters = "cert="+certCegado;
			
		//--------URL---------------//
		String url = ruta_RA;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//CABECERA
		con.setRequestMethod("POST");
    	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    	
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando 'POST' a la  URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Código respuesta : " + responseCode);
		
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println("RESPUESTA---------------------------"+response.toString());

		renderText(response.toString());
		
	}
	
	
	
	
	
	
	
	
	
	
}
	

	
		

		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		

