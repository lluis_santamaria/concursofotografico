package controllers;

import play.*;
import play.mvc.*;

import java.net.MalformedURLException;
import java.util.*;

import notifiers.Mails;

import org.apache.commons.mail.EmailException;

import models.*;
/**
 * CONTROLADOR QUE ESCUCHA PETICIONES HTTP DE LOS HTML
 * Este controlador funciona hasta que un usuario abre el socket con el login
 * 
 * @author Ismael
 * @author Marti
 *
 */
public class Application extends Controller {

	/**
	 * Metodo que recupera al usuario del socket utilizando la key del email
	 * 
	 * @return retornamos un objeto tipo User
	 */
	static User connected() 
	{
		// Abrimos sesion del usuario
		String email = session.get("email");
		User u = new User();

		if (email != null) {
			System.out.println("El email de la persona conectada es " + email);
			u = User.find("byEmail", email).first();

			System.out.println("ID del User conectado " + u.getId_user());

			return u;// identificamos al Usuario de la sesion
		}

		return null;

	}
	/** 
	 * Renderizamos la plantilla index.html y le enviamos el periodo al html 
	 */
	private static Periodos time = null;
	public static void index() 
	{
		Periodos time=Periodos.find("byId_periodo", 0).first();
		System.out.println("Entramos en función index");
		render(time);
	}
	public static void construir_fecha_prova()
	{
		Calendar fecha_inicial_foto=Calendar.getInstance();
		fecha_inicial_foto.set(2016, 1, 1, 00, 00, 00);

		Calendar fecha_final_foto=Calendar.getInstance();
		fecha_final_foto.set(2016, 8, 1, 12, 00, 00);

		Calendar fecha_inicial_voto=Calendar.getInstance();
		fecha_inicial_foto.set(2016, 8, 2, 12, 00, 00);

		Calendar fecha_final_voto=Calendar.getInstance();
		fecha_final_foto.set(2016, 8, 31, 12, 00, 00);

		Periodos p = new Periodos();
		p.setFecha_inicial_subida_foto(fecha_inicial_foto);
		p.setFecha_final_subida_foto(fecha_final_foto);
		p.setFecha_inicial_voto(fecha_inicial_voto);
		p.setFecha_final_voto_foto(fecha_final_voto);
		p.setId_periodo(0);
		p.save();
		
		time = p;

		renderText("ok");

	}
	/**
	 * Renderizamos la plantilla registerTemplate.html
	 */
	public static void registerTemplate() {

		System.out.println("Enviar formulario del register");
		render();
	}
	/**
	 * Renderizamos la plantilla loginTemplate.html
	 */
	public static void loginTemplate() {

		System.out.println("Enviar formulario del login");
		render();
	}
	/**
	 * Metodo que comprueba si un usuario existe en la base de datos y le damos 
	 * permisos de entrada a la plataforma
	 * 
	 * @param email Email del usuario
	 * @param Contrasenya contraseña del usuario
	 */
	public static void login(String email, String contrasenya) {

		System.out.println("Email: " + email);
		System.out.println("Contrasenya: " + contrasenya);

		User u = User.find("byEmail", email).first();
		// System.out.println("Lo comparamos con la contrasenya " +u.getPass());

		if (u != null) 
		{
			// email exists

			if (contrasenya.equals(u.getPass())) {
				// renderizamos la pagina siguiente, login bien hecho
				
				if(u.isMail_validado())
				{
					if(u.getRole().equals("user"))
					{
						
						session.put("email", u.getEmail());
						System.out.println("Login correcto");
						
						if(connected().nfaces==3)
						{
							Server.SetUpTemplate();
						}
						else
						{
							Server.camera();
						}
					}
					else
					{
						//QUIERE ENTRAR EL ADMINISTRADOR, LE ENVIAMOS OTRAS VISTAS
						Administrador.indexA();
					}
				}
				else
				{
					renderText("Tu correo no esta validado, mira la bandeja de Spam");
				}
				
			} 
			else 
			{
				System.out.println("Password doesn't correct");
				loginTemplate();
			}

		}

		// email doesn't exist
		System.out.println("Email no existe");
		loginTemplate();

	}
	/**
	 * Metodo que registra en la base de datos un usuario
	 * 
	 * @param Nombre
	 * @param apellido
	 * @param contrasenya
	 * @param dni
	 * @param email
	 * @throws MalformedURLException Controlamos la formación correcta de una URL
	 * @throws EmailException Controlamos el formato correcto de un email "algo@algo"
	 */
	public static void register(String Nombre, String apellido, String contrasenya, String dni, String email) throws MalformedURLException, EmailException {
	
		System.out.println("******REGISTRAMOS A UN USUARIO******");
		System.out.println("Nombre: " + Nombre);
		System.out.println("Apellido: " + apellido);
		System.out.println("Pass: " + contrasenya);
		System.out.println("DNI: " + dni);
		System.out.println("Email: " + email);

		User u2 = new User();
		User u = new User();
		List<User> LUsers = User.findAll();

		u2 = User.find("byEmail", email).first();
		System.out.println("Usuario: " + u2);
		

		if (u2 == null) // Usuario no existe lo podemos registrar
		{
			Mails m = new Mails();
			u.setFirstname(Nombre);
			u.setLastname(apellido);
			u.setPass(contrasenya);
			u.setEmail(email);
			u.setDni(dni);
			u.setId_user(LUsers.size());
			u.setSubida_foto(false);//NO HA VOTADO
			u.setVoto(false);//NO HA SUBIDO FOTO
			u.setRole("user");
			int check = m.welcome(u);
			
			if(check==0)
			{
				u.save();			
				System.out.println("Usuario: " + u + " Guardado en la DataBase");
				Application.index();
			}
			else
			{
				System.err.println("Error con el mail que ha puesto el usuario");
				renderText("Pon bien un correo electrónico");
			}
									
		} else // Usuario Registrado
		{
			System.out.println("Usuario ya existe");

			registerTemplate();

		}

	}
}