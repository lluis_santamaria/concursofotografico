package controllers;

import play.*;
import play.mvc.*;

import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.*;

import models.*;
/**
 * CONTROLADOR QUE ESCUCHA PETICIONES HTTP DE LOS HTML
 * Este controlador funciona a partir de que un usuario con rol ADMINISTRADOR abre socket
 * 
 * @author Ismael
 *
 */
public class Administrador extends Controller 
{
//	@Before
//    static void checkUser_admin() 
//	{
//        if(Server.connected() == null) 
//        {
//            flash.error("Please log in first");
//           Application.index();
//        }
//    }
	/**
	 * Renderizamos el indexA.html
	 */
	public static void indexA()
	{
		render();
	}
	/**
	 * Metodo que construye un periodo de votación y subida de foto a decision del administrador
	 * 
	 * @param pfotoinicial fecha que viene del html de un input type=date ABRE PERIODO DE SUBIDA DE FOTOS
	 * @param pfotofinal fecha que viene del html de un input type=date CIERRA PERIODO DE SUBIDA DE FOTOS
	 * @param pvotoinicial fecha que viene del html de un input type=date ABRE PERIODO DE VOTACION
	 * @param pvotofinal fecha que viene del html de un input type=date CIERRA PERIODO DE SUBIDA DE FOTOS
	 * @param horaFI Hora que corresponde a la fecha inicio de foto
	 * @param minutoFI minuto que corresponde a la fecha inicio de foto
	 * @param segundoFI segundo que corresponde a la fecha inicio de foto
	 * @param horaFF Hora que corresponde a la fecha final de foto
	 * @param minutoFF minuto que corresponde a la fecha final de foto
	 * @param segundoFF segundo que corresponde a la fecha final de foto
	 * @param horaVI Hora que corresponde a la fecha inicio voto
	 * @param minutoVI minuto que corresponde a la fecha inicio voto
	 * @param segundoVI segundo que corresponde a la fecha inicio voto
	 * @param horaVF Hora que corresponde a la fecha final voto
	 * @param minutoVF Minuto que corresponde a la fecha inicio voto
	 * @param segundoVF Segundo que corresponde a la fecha inicio voto
	 */
	public static  void setupdates(String pfotoinicial, String pfotofinal, String pvotoinicial, String pvotofinal, 
			String horaFI, String minutoFI, String segundoFI, String horaFF, String minutoFF, String segundoFF,
			String horaVI, String minutoVI, String segundoVI, String horaVF, String minutoVF, String segundoVF)
	{
		
		int horaFIint, minutoFIint, segundoFIint, horaFFint, minutoFFint, segundoFFint,
			horaVIint, minutoVIint, segundoVIint, horaVFint, minutoVFint, segundoVFint;
		
//		SimpleDateFormat formato = new SimpleDateFormat ("YYYY-MM-DD:HH:mm:ss");
//		String fechaTotalFotoInicial,fechaTotalFotoFinal,fechaTotalVotoInicial,FechaTotalVotoFinal;
		
		
		horaFIint = Integer.parseInt(horaFI);
		minutoFIint=  Integer.parseInt(minutoFI);
		segundoFIint=Integer.parseInt( segundoFI);
		horaFFint= Integer.parseInt(horaFF);
		minutoFFint= Integer.parseInt(minutoFF);
		segundoFFint= Integer.parseInt(segundoFF);
		horaVIint= Integer.parseInt(horaVI);
		minutoVIint=Integer.parseInt( minutoVI);
		segundoVIint = Integer.parseInt(segundoVI);
		horaVFint= Integer.parseInt(horaVF);
		minutoVFint = Integer.parseInt(minutoVF);
		segundoVFint= Integer.parseInt(segundoVF); 
		
		System.out.println(" "+horaFIint+" "+ minutoFIint+ " "+segundoFIint+" "+ horaFFint+ " "+minutoFFint+" "+segundoFFint+
				" "+horaVIint+" "+ minutoVIint+" "+ segundoVIint+ " "+horaVFint+ " "+minutoVFint+" "+ segundoVFint);
		
//		fechaTotalFotoInicial= pfotoinicial+":"+horaFI+":"+minutoFI+":"+segundoFI;
//		fechaTotalFotoFinal = pfotofinal+":"+horaFF+":"+minutoFF+":"+segundoFF;
//		fechaTotalVotoInicial= pvotoinicial+":"+horaVI+":"+minutoVI+":"+segundoVI;
//		fechaTotalFotoFinal = pvotofinal+":"+horaVF+":"+minutoVF+":"+segundoVF;
//		
		
		
		String[] fecha1=pfotoinicial.split("-");
		Calendar fecha_inicial_foto=Calendar.getInstance();
		
		fecha_inicial_foto.set(Integer.parseInt(fecha1[0]),Integer.parseInt(fecha1[1])-1, Integer.parseInt(fecha1[2]),
				horaFIint,minutoFIint, segundoFIint);
		
		System.out.println("fecha inicial foto: "+pfotoinicial+", "+fecha1[0]+" "+fecha1[1]+" "+fecha1[2]+" ");
		System.out.println("fecha inicial foto: "+fecha_inicial_foto.getTime());
		
		String[] fecha2=pfotofinal.split("-");
		Calendar fecha_final_foto=Calendar.getInstance();
		fecha_final_foto.set(Integer.parseInt(fecha2[0]),Integer.parseInt(fecha2[1])-1, Integer.parseInt(fecha2[2]),
				horaFFint,minutoFFint, segundoFFint);
		
		System.out.println("fecha final foto: "+pfotofinal);
		System.out.println("fecha final foto: "+fecha_final_foto.getTime());
		
		String[] fecha3=pvotoinicial.split("-");
		Calendar fecha_inicial_voto=Calendar.getInstance();
		fecha_inicial_voto.set(Integer.parseInt(fecha3[0]),Integer.parseInt(fecha3[1])-1, Integer.parseInt(fecha3[2]),
				horaVIint,minutoVIint, segundoVIint);
		
		
		
		//System.out.println("fecha inicial voto: "+pvotoinicial);
		System.out.println("fecha inicial voto: "+fecha_inicial_voto.getTime());
		
		String[] fecha4=pvotofinal.split("-");
		Calendar fecha_final_voto=Calendar.getInstance();
		fecha_final_voto.set(Integer.parseInt(fecha4[0]), Integer.parseInt(fecha4[1])-1, Integer.parseInt(fecha4[2]),
		horaVFint, minutoVFint, segundoVFint);
		
		System.out.println("fecha final voto: "+pvotofinal);
		System.out.println("fecha final voto: "+fecha_final_voto.getTime());
		
		Periodos p2 = Periodos.find("byId_periodo", 0).first();
		
		if(p2!=null)
		{
			p2.delete();
			Periodos p = new Periodos();
			p.setFecha_inicial_subida_foto(fecha_inicial_foto);
			p.setFecha_final_subida_foto(fecha_final_foto);
			p.setFecha_inicial_voto(fecha_inicial_voto);
			p.setFecha_final_voto_foto(fecha_final_voto);
			p.setId_periodo(0);
			p.save();
		
			renderText("ok");
		}
		else
		{
			Periodos p = new Periodos();
			p.setFecha_inicial_subida_foto(fecha_inicial_foto);
			p.setFecha_final_subida_foto(fecha_final_foto);
			p.setFecha_inicial_voto(fecha_inicial_voto);
			p.setFecha_final_voto_foto(fecha_final_voto);
			p.setId_periodo(0);
			p.save();
		}
	}
}
