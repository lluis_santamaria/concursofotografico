package controllers;
import java.applet.AppletContext;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Point;
import javax.swing.JOptionPane;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;


import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;


import sun.net.www.http.HttpClient;
import sun.security.mscapi.SunMSCAPI;

import javax.crypto.Cipher;
import javax.net.ssl.HttpsURLConnection;

//import org.bouncycastle.asn1.ASN1InputStream;
//import org.bouncycastle.asn1.DERObject;
//import org.bouncycastle.asn1.DEROctetString;
//import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


import org.apache.commons.codec.binary.Base64;



import java.security.cert.*;
import java.security.interfaces.RSAPublicKey;
import java.io.*;

import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.KeyStoreException;

/**
 * 
 * Applet para registrarse a través de los certificados
 * @author Lluis Santamaria Solà
 *
 */

public class LoginApplet extends JApplet 
{
	//Creamos una lista de botones ya que habrá tantos como certificados. La hacemos
	// global y pública para poder acceder a ella con el fin de iniciar el listener para
	//cada botón
	
	/**
	 * Variables globles para poderlas modificar
	 * 
	 */
	final static String direccion_concurso = "http://192.168.1.129:9999/security/register_con_certificado";
	final static String ruta_cert_invalido = "http://192.168.1.129:9999/application/register_Template";
	final static String ruta_registro = "http://192.168.1.129:9999/application/certificat";
	public List<JButton> lista_botones = new ArrayList();

	//Definimos una lista para asignar los certificados encontrados
	//public List<Certificate> certificados = new ArrayList();
	

	public List<X509Certificate> certificados = new ArrayList();
	public List<String> nombres_certs= new ArrayList();
	int numero_botones=0;
	String certificado_seleccionado;


	public void init() 
	{

		try {
			javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					createGUI();
				}
			});
		} catch (Exception e) {
			System.err.println("createGUI didn't successfully complete");

		}         	




		cargar_certificados();
		iniciar_listener();

	}


	public void createGUI()
	{

		setLayout(new GridLayout(numero_botones,1));
		setSize(300,300);

	}


	/*----------OBTENCIÓN DE CERTIFICADOS DE LA CRYPTOAPI DE WINDOWS--------*/

	public void cargar_certificados()
	{

		KeyStore ks=null;
		Enumeration nombres_certificados =null;
		int i = 0;
		int y = 0;

		try{


			ks = KeyStore.getInstance("Windows-MY");
			ks.load(null, null);

				
			//Obtenemos los nombres de los certificados
			nombres_certificados = ks.aliases();


		} catch (KeyStoreException k) {
			// TODO Auto-generated catch block
			k.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 


		String nombre_cert;



		String h = "";


		/*-------------OBTENCIÓN Y ASIGNACIÓN DE NOMBRES DE CERTIFICADOS----------*/

		while(nombres_certificados.hasMoreElements())
		{

			//Obtenemos cada certificado según el nombre de la enumeración definida, la cual
			//contiene los nombres de todos los certificados encontrados en la CryptoAPI

			nombre_cert= nombres_certificados.nextElement().toString();

			//Lo añadimos a la lista de nombres

			nombres_certs.add(nombre_cert);

			try {

				//Obtenemos certificado
				X509Certificate cer = (X509Certificate)ks.getCertificate(nombre_cert);


				certificados.add(cer);

				//X509Certificate certx = convert(cer);



			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




		}



		/*-CREACIÓN DE BOTONES PARA CADA CERTIFICADO-*/

		//Creamos tantos botones como certificados hayamos encontrados para poder seleccionar
		//uno con el fin de demostrar que somos nosotros para registrarnos en la aplicación
		System.out.println("HAy "+nombres_certs.size());


		//Creamos una lista de botones (cada uno corresponderá a un certificado)

		i = 0;
		y = 0;
		while(i<nombres_certs.size())
		{
			JButton b= new JButton(nombres_certs.get(i));
			Dimension d = new Dimension(900,900);
			b.setMaximumSize(d);
			b.setLocation(100, y);


			lista_botones.add(b);

			add(b);

			i++;
			y = y +100;
		}



	}

	/*----------------------CONVERSIÓN DE CERTIFICADO Certificate a X509Certificate----------------*/

	public static java.security.cert.X509Certificate convert(Certificate cert) {
		try {
			byte[] encoded = cert.getEncoded();
			ByteArrayInputStream bis = new ByteArrayInputStream(encoded);
			java.security.cert.CertificateFactory cf
			= java.security.cert.CertificateFactory.getInstance("X.509");

			return (java.security.cert.X509Certificate)cf.generateCertificate(bis);

		} catch (java.security.cert.CertificateEncodingException e) {
		} catch (java.security.cert.CertificateException e) {
		}
		return null;
	}


	//Iniciamos el listener desde el init(), para asignar a cada botón 
	// un listener propio

	public void iniciar_listener()
	{
		int i = 0;


		while(i<lista_botones.size())
		{
			lista_botones.get(i).addActionListener(new ListenerButton());

			i++;
		}
	}



	/*-------------OBTENCIÓN DE CERTIFICADO SEGÚN SU NOMBRE--------------------*/

	public X509Certificate buscar_certificado_por_nombre(String n)
	{

		int i = 0;
		boolean encontrado =false;

		while (i<certificados.size() && !encontrado)
		{
			if(n.equals(nombres_certs.get(i)))
			{
				encontrado = true;
			}

			else 
				i++;

		}

		if(encontrado)
		{
			return certificados.get(i);
		}
		else
			return null;
	}

	public class ListenerButton implements ActionListener 
	{


		public void actionPerformed (ActionEvent event)
		{

			//Obtenemos nombre de certificado
			String a = event.toString();
			String nombre_cert = event.getActionCommand();

			System.out.println(a+"\n nom es "+nombre_cert);


			//Buscamos el certificado
			X509Certificate c = buscar_certificado_por_nombre(nombre_cert);

			if( c != null)
			{

				try 
				{
					sendPost(c);

				} 

				catch (Exception e) 
				{

					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
			else 
				System.out.println("CERTIFICADO NO ENCONTRADO EN KEYSTORE");


		}



	}
	
	/**
	 * Método para enviar el certificado a validación
	 * @param cert_x509
	 * @throws Exception
	 */
		
	protected void sendPost(X509Certificate cert_x509) throws Exception 
	{
		
		
		String cert;
		System.out.println("Enviando certificado al servidor....");
		
		System.out.println("CERTIFICAT TIPUSSSSSSSSSSS-------"+cert_x509.getSigAlgName());
		System.out.println("CERTIFICAT TIPUSSSSSSSSSSS-------"+cert_x509.getSigAlgName());
		System.out.println("CERTIFICAT TIPUSSSSSSSSSSS-------"+cert_x509.getTBSCertificate());
		System.out.println("CERTIFICAT EN STRING ES \n : "+cert_x509.toString());
		
		byte[] certificado_codificado = cert_x509.getEncoded();
		System.out.println("\n\n*****************\n LONGITUD CERTIFICAT ABANS D'ENVIAR: "+certificado_codificado.length);
		
		
		FileOutputStream fos = new FileOutputStream("C:/cer/certificat.cer");
		
		fos.write(certificado_codificado);
		fos.flush();
		fos.close();
		
		System.out.println("cert abans codificat:"+certificado_codificado);
		
		
		
		
		byte [] certificado_envio = Base64.encodeBase64(certificado_codificado);
		
		String stringMessage = new String(certificado_envio,"UTF-8");  
		stringMessage.replaceAll(" ", "+");
		
		System.out.println("*****ENCRYPTED MESSAGE String Length: "+stringMessage.length());
		
		
		byte[] p = Base64.decodeBase64(stringMessage.getBytes());
		
				
		//****????*****------PARaMETROS A ENVIAR----------------//
		
		String urlParameters = "cert="+stringMessage;
		
				
		//--------URL---------------//
		String url = direccion_concurso;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//CABECERA
		con.setRequestMethod("POST");
    	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    	
		// Envío POST
		con.setDoOutput(true);
		con.setFollowRedirects(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		//ANADIENDO PARAMETROS Y ENVIANDO
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		con.setFollowRedirects(true);
		
		//RECIBIENDO RESPUESTA
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(	new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		if("0".equals(response.toString()))
		{
			
		
			//REDIRECCIÓN SEGÚN RESPUESTA SERVIDOR
			AppletContext ap = getAppletContext();
			URL u = new URL(ruta_registro+"?DNI="+(cert_x509.getSerialNumber().toString()));
			ap.showDocument(u);
		}
		
		else
		{
			AppletContext ap = getAppletContext();
			URL u = new URL(ruta_cert_invalido);
			ap.showDocument(u);
		}

	}




	public static X509Certificate readWWDRCertificate(String keyFile) throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException
	{
		FileInputStream fis = null;
		ByteArrayInputStream bais = null;
		try 
		{
			// use FileInputStream to read the file
			fis = new FileInputStream(keyFile);

			// read the bytes
			byte value[] = new byte[fis.available()];
			fis.read(value);
			bais = new ByteArrayInputStream(value);

			// get X509 certificate factory
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");

			// certificate factory can now create the certificate 
			return (X509Certificate)certFactory.generateCertificate(bais);
			
		}
		finally 
		{
			fis.close();
			bais.close();

			/*IOUtils.closeQuietly(fis);
	  IOUtils.closeQuietly(bais);*/
		}
	}

	private void enviar_cert_invalido()
	{

		String url = ruta_cert_invalido;
		URL obj;
		try {
			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setDoOutput(true);
			con.setFollowRedirects(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.flush();
			wr.close();
			con.setFollowRedirects(true);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();


			//print result
			System.out.println("resposta---------------------------------"+response.toString());

			AppletContext a = getAppletContext();
			URL u = new URL(response.toString());
			a.showDocument(u);


		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	





}